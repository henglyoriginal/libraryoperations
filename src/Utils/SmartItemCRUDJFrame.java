package Utils;

import Admin.Items.ItemDetailForm;
import org.jooq.Record;
import org.jooq.Result;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ItemEvent;

import static Database.generated.tables.DivisionSubjects.DIVISION_SUBJECTS;
import static Database.generated.tables.Items.ITEMS;
import static Database.generated.tables.Subjects.SUBJECTS;

public abstract class SmartItemCRUDJFrame extends SmartCRUDJFrame {

    protected abstract String getPrefix();

    protected JTextField txtItemNumber;
    protected JTextField txtTitle;
    protected JComboBox cboMainSubject;
    protected JComboBox cboDivisionSubject;
    private JLabel lblAmount;
    private JLabel lblPrefix;
    private Result<Record> mainSubjects;
    private Result<Record> divisionSubjects;

    public SmartItemCRUDJFrame(Component parent) throws HeadlessException {
        super(parent);
    }

    protected void initializeTableComponents(
            JTable table,
            String[] columnNames,
            int[] columnToBeRemovedIndices,
            ValueWrapper[] searchableColumn,
            JButton btnNewOrAdd,
            JButton btnUpdate,
            JButton btnDelete,
            JTextField txtItemNumber,
            JTextField txtTitle,
            JComboBox cboMainSubject,
            JComboBox cboDivisionSubject,
            JLabel lblAmount,
            JLabel lblPrefix
    ) throws HeadlessException {
        initializeTableComponents(table, columnNames, columnToBeRemovedIndices, searchableColumn, btnNewOrAdd, btnUpdate, btnDelete, null, null, null, txtItemNumber, txtTitle, cboMainSubject, cboDivisionSubject, lblAmount, lblPrefix);
    }

    protected void initializeTableComponents(
            JTable table,
            String[] columnNames,
            int[] columnToBeRemovedIndices,
            ValueWrapper[] searchableColumn,
            JButton btnNewOrAdd,
            JButton btnUpdate,
            JButton btnDelete,
            JButton btnDetail,
            JComboBox cboSearch,
            JTextField txtSearch,
            JTextField txtItemNumber,
            JTextField txtTitle,
            JComboBox cboMainSubject,
            JComboBox cboDivisionSubject,
            JLabel lblAmount,
            JLabel lblPrefix
    ) throws HeadlessException {
        this.txtItemNumber = txtItemNumber;
        this.txtTitle = txtTitle;
        this.cboMainSubject = cboMainSubject;
        this.cboDivisionSubject = cboDivisionSubject;
        this.lblAmount = lblAmount;
        this.lblPrefix = lblPrefix;

        cboMainSubject.addItemListener(e -> {
            if (e.getStateChange() == ItemEvent.SELECTED) {
                divisionSubjects = PopulateUtils.populateDivisionSubject(cboDivisionSubject, ((ComboItem<String, Integer>) e.getItem()).getValue());
            }
        });
        mainSubjects = PopulateUtils.populateMainSubject(cboMainSubject);
        super.initializeTableComponents(table, columnNames, columnToBeRemovedIndices, searchableColumn, btnNewOrAdd, btnUpdate, btnDelete, btnDetail, cboSearch, txtSearch);
    }

    @Override
    protected void setContent(Record record) {
        txtItemNumber.setText(record.getValue(ITEMS.ITEM_CODE));
        txtTitle.setText(record.getValue(ITEMS.TITLE));
        cboMainSubject.setSelectedIndex(findItemIndexInModelByCode(mainSubjects, record.getValue(SUBJECTS.CODE), SUBJECTS.CODE));
        cboDivisionSubject.setSelectedIndex(findItemIndexInModelByCode(divisionSubjects, record.getValue(DIVISION_SUBJECTS.CODE), DIVISION_SUBJECTS.CODE));
        Short ownedCopies = record.get(ITEMS.OWNED_COPIES);
        lblAmount.setText(String.valueOf((ownedCopies - record.getValue(ITEMS.BORROWED_COPIES))).concat("/").concat(ownedCopies.toString()));
    }

    @Override
    protected void clearControl() {
        ControlUtils.clearControls(
                txtItemNumber,
                txtTitle
        );
        if (cboMainSubject.getItemCount() > 0) cboMainSubject.setSelectedIndex(0);
        if (cboDivisionSubject.getItemCount() > 0) cboDivisionSubject.setSelectedIndex(0);
        lblAmount.setText("0/0");
    }

    @Override
    protected void toggleControls(boolean enabled) {
        ControlUtils.toggleControl(
                enabled,
                txtTitle,
                cboMainSubject,
                cboDivisionSubject
        );
    }

    @Override
    protected boolean validateControls() {
        return ControlUtils.validateControl(this,
                new ValidationWrapper(txtTitle, component -> !"".equals(((JTextField)component).getText()), "Title can't be empty")
        );
    }

    @Override
    protected Runnable getDetailAction() {
        return () -> {
            FormSwitching.switchForm(
                    SmartItemCRUDJFrame.this,
                    ItemDetailForm.class,
                    getColumnOfSelectedRow(table, 0).toString()
            );
            table.clearSelection();
        };
    }

    @Override
    protected Runnable getSearchIndexChangeAction() {
        return () -> lblPrefix.setText(cboSearch.getSelectedItem().toString().equals("Item Code") ? ( getPrefix() + "-") : "   ");
    }
}
