package Utils;

import Admin.Application.Main;
import Database.generated.tables.records.ItemsRecord;
import org.jooq.Field;
import org.jooq.Record;
import org.jooq.Result;
import org.jooq.TableField;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;
import java.awt.*;
import java.awt.event.ItemEvent;
import java.util.HashMap;
import java.util.Map;

public abstract class SmartCRUDJFrame extends SmartJFrame {

    protected abstract <T, R extends Record> TableField<R, T> getPrimaryField();
    protected abstract Result<Record> refreshTableAndModel(DefaultTableModel tableModel);
    protected abstract void clearControl();
    protected abstract void toggleControls(boolean enabled);
    protected abstract boolean validateControls();
    protected abstract void setContent(Record record);
    protected abstract Runnable getAddItemAction();
    protected abstract Runnable getUpdateItemAction();
    protected abstract Runnable getDeleteItemAction();

    //Override Me
    protected Runnable getSearchIndexChangeAction() {
        return () -> {};
    }
    protected Runnable getDetailAction() {
        return () -> {};
    }
    protected boolean getAddCustomValidation() {
        return true;
    }

    protected boolean getUpdateCustomValidation() {
        return true;
    }

    protected boolean getDeleteCustomValidation() {
        return AuthenticateUtils.isAuthenticated(SmartCRUDJFrame.this, "admin", "admin's");
    }
    protected void onNewClickAfterControlToggle() {}
    protected void onTableItemSelectedAfterToggle() {}

    protected JTable table;
    private DefaultTableModel tableModel;
    private Result<Record> model;
    private String[] columnNames;
    private int[] columnToBeRemovedIndices;
    private JButton btnNewOrAdd;
    private JButton btnUpdate;
    private JButton btnDelete;
    private JButton btnDetail;
    protected JComboBox cboSearch;
    private JTextField txtSearch;
    protected Map<String, Field<String>> searchMapper;
    protected int toBeSelectedIndex = -1;

    public SmartCRUDJFrame(Component parent) throws HeadlessException {
        super(parent);
    }

    protected void initializeTableComponents(
            JTable table,
            String[] columnNames,
            int[] columnToBeRemovedIndices,
            JButton btnNewOrAdd,
            JButton btnUpdate,
            JButton btnDelete
    ) throws HeadlessException {
        initializeTableComponents(table, columnNames, columnToBeRemovedIndices, null, btnNewOrAdd, btnUpdate, btnDelete, null, null, null);
    }

    protected void initializeTableComponents(
            JTable table,
            String[] columnNames,
            int[] columnToBeRemovedIndices,
            ValueWrapper[] searchableColumn,
            JButton btnNewOrAdd,
            JButton btnUpdate,
            JButton btnDelete,
            JButton btnDetail,
            JComboBox cboSearch,
            JTextField txtSearch
    ) throws HeadlessException {
        this.table = table;
        this.tableModel = new DefaultTableModel();
        this.columnNames = columnNames;
        this.columnToBeRemovedIndices = columnToBeRemovedIndices;
        this.btnNewOrAdd = btnNewOrAdd;
        this.btnUpdate = btnUpdate;
        this.btnDelete = btnDelete;
        this.btnDetail = btnDetail;
        this.cboSearch = cboSearch;
        this.txtSearch = txtSearch;
        if (null != cboSearch && null != txtSearch) {
            searchMapper = new HashMap<>();
            for (ValueWrapper valueWrapper : searchableColumn) {
                searchMapper.put(valueWrapper.getKey(), valueWrapper.getValue());
            }
        }

        initialize();
    }

    protected void initialize() {
        table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        for (String columnName : columnNames) {
            tableModel.addColumn(columnName);
        }
        model = refreshTableAndModel(tableModel);
        table.setModel(tableModel);

        for (int columnToBeRemovedIndex : columnToBeRemovedIndices) {
            TableColumnModel tableColumnModel = table.getColumnModel();
            tableColumnModel.removeColumn(tableColumnModel.getColumn(columnToBeRemovedIndex));
        }

        table.getSelectionModel().addListSelectionListener(e -> {
            if (table.getSelectionModel().isSelectionEmpty()) {
                clearControl();
                if ("New".equals(btnNewOrAdd.getText()))  {
                    toggleControls(false);
                    if (null != btnDetail) btnDetail.setEnabled(false);
                }
            }
            else {
                toggleControls(true);
                onTableItemSelectedAfterToggle();
                if (null != btnDetail) btnDetail.setEnabled(true);
                refreshContent();
                controlConfiguration("New", true);
                toBeSelectedIndex = table.convertRowIndexToView(
                        findItemIndexInModelByCode(model, getColumnOfSelectedRow(table, 0).toString(), getPrimaryField())
                );
            }
        });

        initializeAddActionListener();
        initializeUpdateActionListener();
        initializeDeleteActionListener();
        initializeDetailActionListener();
        initializeSearchComponent();

    }

    private void initializeAddActionListener() {
        btnNewOrAdd.addActionListener(e -> {
            String text = btnNewOrAdd.getText();
            if ("New".equals(text)) {
                clearControl();
                toggleControls(true);
                onNewClickAfterControlToggle();
                if (null != btnDetail) btnDetail.setEnabled(false);
                controlConfiguration("Add", false);
                table.getSelectionModel().clearSelection();
            } else if ("Add".equals(text)) {
                if (validateControls() && getAddCustomValidation()) {
                    try {
                        getAddItemAction().run();
                        DialogUtils.showInfoMsg(this, "Successfully Added");
                        clearControl();
                        model = refreshTableAndModel(tableModel);
                        Object lastInsertedId = model
                                .get(model.size()-1)
                                .getValue(getPrimaryField());
                        toBeSelectedIndex = findItemIndexInModelByCode(model, lastInsertedId, getPrimaryField());
                        selectTableAtToBeIndex();
                    } catch (Exception e1) {
                        e1.printStackTrace();
                        DialogUtils.showErrorMsg(this, e1.getMessage());
                    }
                }
            }
        });
    }

    private void initializeUpdateActionListener() {
        btnUpdate.addActionListener(e -> {
            if (validateControls() && getUpdateCustomValidation()) {
                getUpdateItemAction().run();
                model = refreshTableAndModel(tableModel);
                selectTableAtToBeIndex();
                DialogUtils.showInfoMsg(this, "Successfully updated");
            }
        });
    }

    private void initializeDeleteActionListener() {
        btnDelete.addActionListener(e -> {
            if (getDeleteCustomValidation()) {
                getDeleteItemAction().run();
                DialogUtils.showInfoMsg(this, "Successfully deleted");
                model = refreshTableAndModel(tableModel);
                putFormInStartMode();
            }
        });
    }

    private void initializeDetailActionListener() {
        if (null != btnDetail) btnDetail.addActionListener(e -> {
            getDetailAction().run();
        });
    }


    private void initializeSearchComponent() {
        if (null != txtSearch) {
            txtSearch.getDocument().addDocumentListener(new DocumentListener() {
                @Override
                public void insertUpdate(DocumentEvent e) {
                    action();
                }

                @Override
                public void removeUpdate(DocumentEvent e) {
                    action();
                }

                @Override
                public void changedUpdate(DocumentEvent e) {

                }

                private void action() {
                    model = refreshTableAndModel(tableModel);
                }
            });
        }

        if (null != cboSearch) {
            cboSearch.addItemListener(e -> {
                if (e.getStateChange() == ItemEvent.SELECTED) {
                    getSearchIndexChangeAction().run();
                    model = refreshTableAndModel(tableModel);
                    putFormInStartMode();
                }
            });
        }
    }

    private void putFormInStartMode() {
        toggleControls(false);
        controlConfiguration("New", false);
        if (null != btnDetail) btnDetail.setEnabled(false);
    }

    private void controlConfiguration(String text, boolean enabled) {
        btnNewOrAdd.setText(text);
        btnUpdate.setEnabled(enabled);
        btnDelete.setEnabled(enabled);
    }

    private void refreshContent() {
        if (!table.getSelectionModel().isSelectionEmpty() && null != model) {
            Record record = findItemInModelByCode(getColumnOfSelectedRow(table, 0), getPrimaryField());
            setContent(record);
        }
    }

    private <T> Record findItemInModelByCode(T code, TableField<ItemsRecord, T> field) {
        int index = findItemIndexInModelByCode(model, code, field);
        return -1 != index ? model.get(index) : null;
    }

    @Override
    public void setVisible(boolean b) {
        super.setVisible(b);
        if (b) model = refreshTableAndModel(tableModel);
        selectTableAtToBeIndex();
    }

    private void selectTableAtToBeIndex() {
        if (-1 != toBeSelectedIndex) table.getSelectionModel().setSelectionInterval(toBeSelectedIndex, toBeSelectedIndex);
    }

    protected Result<Record> getModel() {
        return model; //TODO: change ot unmodifiable later
    }
}
