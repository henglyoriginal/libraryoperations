package Utils;

import Admin.EmployeeForm;
import Database.generated.Routines;

import java.awt.*;

import static Admin.Application.Main.database;

public class AuthenticateUtils {

    public static boolean isAuthenticated(Component parent, String who, String whose) {
        String thatEmployeePassword = DialogUtils.prompt(parent, "Enter " + whose + " password:");
        if (null != thatEmployeePassword) {
            String passwordHash = Hasher.hash(thatEmployeePassword);
            if (!"".equals(thatEmployeePassword)) {
                if (!"-1".equals(database.context().select(
                        Routines.authenticate(who, passwordHash)
                ).fetchOne().getValue(0).toString())) {
                    return true;
                } else {
                    DialogUtils.showErrorMsg(parent, "Invalid password");
                }
            } else {
                DialogUtils.showErrorMsg(parent, "Password can't be empty");
            }
        }
        return false;
    }

}
