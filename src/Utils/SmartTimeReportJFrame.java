package Utils;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ItemEvent;

public abstract class SmartTimeReportJFrame extends SmartReportJFrame {

    protected abstract Runnable getYearPopulator();
    protected abstract Runnable getMonthPopulator();

    public static final String MONTHLY = "Monthly";
    public static final String YEARLY = "Yearly";
    private JComboBox cboYear;
    private JComboBox cboMonth;
    private JComboBox cboPreference;

    public SmartTimeReportJFrame(Component parent) throws HeadlessException {
        super(parent);
    }

    protected void initialize(JTable tblData, JButton btnExport, JComboBox cboYear, JComboBox cboMonth) {
        initialize(tblData, btnExport, cboYear, cboMonth, null);
    }

    protected void initialize(JTable tblData, JButton btnExport, JComboBox cboYear, JComboBox cboMonth, JComboBox cboPreference) {
        this.cboYear = cboYear;
        this.cboMonth = cboMonth;
        this.cboPreference = cboPreference;

        if (null == cboPreference) {
            cboYear.addItemListener(e -> {
                if (e.getStateChange() == ItemEvent.SELECTED) getMonthPopulator().run();
            });
            getYearPopulator().run();

            super.initialize(tblData, btnExport);
            cboMonth.addItemListener(e -> {
                if (e.getStateChange() == ItemEvent.SELECTED) refreshDataAndTable();
            });
        } else {
            cboPreference.addItemListener(e -> {
                if (e.getStateChange() == ItemEvent.SELECTED) {
                    cboMonth.setEnabled(MONTHLY.equals(SmartTimeReportJFrame.this.getSelectedPreference()));
                    SmartTimeReportJFrame.this.getYearPopulator().run();
                }
            });
            PopulateUtils.populatePreference(cboPreference);

            super.initialize(tblData, btnExport);
            cboYear.addItemListener(e -> {
                if (e.getStateChange() == ItemEvent.SELECTED) {
                    getMonthPopulator().run();
                    if (YEARLY.equals(getSelectedPreference())) refreshDataAndTable();
                }
            });
            cboMonth.addItemListener(e -> {
                if (e.getStateChange() == ItemEvent.SELECTED && MONTHLY.equals(getSelectedPreference())) refreshDataAndTable();
            });
        }
    }

    private void refreshDataAndTable() {
        refreshData();
        refreshTable();
    }

    protected Integer getSelectedYear() {
        return Integer.valueOf(cboYear.getSelectedItem().toString());
    }

    protected Integer getSelectedMonth() {
        return Integer.valueOf(cboMonth.getSelectedItem().toString());
    }

    protected String getSelectedPreference() {
        return cboPreference.getSelectedItem().toString();
    }
}
