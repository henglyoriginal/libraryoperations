package Utils;

import org.jooq.Record;
import org.jooq.Result;
import org.jooq.TableField;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public abstract class SmartJFrame extends JFrame implements IForm{

    protected Component parent;
    protected SmartDatePicker datePicker;

    public SmartJFrame(Component parent) throws HeadlessException {
        this.parent = parent;
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                DialogUtils.yesNoDialog(SmartJFrame.this,
                        "Are you sure?",
                        "Confirm",
                        () -> setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE),
                        () -> setDefaultCloseOperation(DO_NOTHING_ON_CLOSE)
                );
            }
        });
    }

    protected SmartDatePicker initializeDatePicker(JPanel datePickerLayout) {
        SmartFontDatePickerSettings datePickerSettings = new SmartFontDatePickerSettings(
                new Font("Google Sans", Font.PLAIN, 16)
        );
        SmartDatePicker datePicker = new SmartDatePicker(datePickerSettings);
        datePickerLayout.add(datePicker, "Center");
        datePicker.setEnabled(false);
        this.datePicker = datePicker;

        return datePicker;
    }

    protected void addBackListener(JComponent btnBack) {
        btnBack.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                goBack();
            }
        });
    }

    protected void goBack() {
        if (null != parent) parent.setVisible(true);
        dispose();
    }

    protected Object getColumnOfSelectedRow(JTable table, int columnIndex) {
        return table.getModel().getValueAt(table.convertRowIndexToModel(table.getSelectedRow()), columnIndex);
    }

    protected final <T, R extends Record> int findItemIndexInModelByCode(Result<Record> model, T code, TableField<R, T> field) {
        if (null == model) return -1;
        for (int i = 0; i < model.size(); i++) {
            if (model.get(i).get(field).toString().equals(code.toString())) return i;
        }
        return -1;
    }

}
