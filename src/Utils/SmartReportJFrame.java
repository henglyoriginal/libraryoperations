package Utils;

import org.jooq.Result;
import ro.nextreports.engine.FluentReportRunner;
import ro.nextreports.engine.Report;

import javax.swing.*;
import javax.swing.table.AbstractTableModel;
import java.awt.*;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

import static Admin.Application.Main.database;

public abstract class SmartReportJFrame extends SmartJFrame {

    protected abstract String getReportName();
    protected abstract Result<?> readData();
    protected Map<String, Object> getParameterValues() {
        return new HashMap<>();
    }

    private JTable tblData;
    private JButton btnExport;
    private Result<?> data;

    public SmartReportJFrame(Component parent) throws HeadlessException {
        super(parent);
    }

    protected void initialize(JTable tblData, JButton btnExport) {
        this.tblData = tblData;
        this.btnExport = btnExport;
        initialize();
    }

    private void initialize() {
        refreshData();
        refreshTable();

        btnExport.addActionListener(e -> {
            OutputStream output = null;
            try {
                Report report = ReportUtils.getReport(getReportName());
                String filename = report.getName().substring(0, report.getName().lastIndexOf('.')) + LocalDateTime.now().format(DateTimeFormatter.ofPattern("@ddMMyyyyhhmmss"));
                String filePath = "output\\" + filename;
                filePath += ".pdf";
                output = new FileOutputStream(filePath);
                FluentReportRunner.report(report)
                        .connectTo(database.getConnection())
                        .withQueryTimeout(60)
                        .withParameterValues(getParameterValues())
                        .formatAs("PDF")
                        .run(output);
                DialogUtils.showInfoMsg(SmartReportJFrame.this, "Created filePath '" + filename + "' in output.");
            } catch (Exception e1) {
                DialogUtils.showErrorMsg(SmartReportJFrame.this, e1.getMessage());
            } finally {
                ReportUtils.closeStream(output);
            }
        });
    }

    protected final void refreshData() {
        data = readData();
    }

    protected void refreshTable() {
        AbstractTableModel tbmData = new AbstractTableModel() {
            @Override
            public int getRowCount() {
                return data.size();
            }

            @Override
            public int getColumnCount() {
                return data.fieldsRow().size();
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                return data.get(rowIndex).get(columnIndex);
            }

            @Override
            public String getColumnName(int column) {
                return data.field(column).getName();
            }
        };

        tblData.setModel(tbmData);
    }

    public Result<?> getData() {
        return data;
    }
}
