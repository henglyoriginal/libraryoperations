package Utils;

import ro.nextreports.engine.queryexec.IdName;

import java.io.Serializable;

public class CustomIdName extends IdName {
    public CustomIdName(Serializable id, Serializable name) {
        setId(id);
        setName(name);
    }
}
