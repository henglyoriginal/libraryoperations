package Utils;

import java.awt.*;
import java.lang.reflect.InvocationTargetException;

public class FormSwitching {

    public static <T extends SmartJFrame> void switchForm(Component from, Class<T> to) {
        switchForm(from, to, "");
    }

    public static <T extends SmartJFrame> void switchForm(Component from, Class<T> to, String id) {
        from.setVisible(false);
        try {
            if (!"".equals(id)) to.getConstructor(Component.class, String.class).newInstance(from, id);
            else to.getConstructor(Component.class).newInstance(from);
        } catch (InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
            e.printStackTrace();
        }
    }
}
