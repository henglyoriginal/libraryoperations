package Utils;

import org.jooq.*;
import org.jooq.impl.DSL;
import org.jooq.impl.TableImpl;

import javax.swing.*;

import java.sql.Timestamp;
import java.time.LocalDateTime;

import static Admin.Application.Main.database;
import static Database.generated.tables.Authors.AUTHORS;
import static Database.generated.tables.Books.BOOKS;
import static Database.generated.tables.BuyTransactions.*;
import static Database.generated.tables.DivisionSubjects.DIVISION_SUBJECTS;
import static Database.generated.tables.Educations.EDUCATIONS;
import static Database.generated.tables.Histories.*;
import static Database.generated.tables.Items.ITEMS;
import static Database.generated.tables.MemberCategories.MEMBER_CATEGORIES;
import static Database.generated.tables.Members.*;
import static Database.generated.tables.Publishers.PUBLISHERS;
import static Database.generated.tables.Subjects.SUBJECTS;

public class PopulateUtils {
    public static Result<Record> populateEducation(JComboBox cboEducation) {
        Result<Record> educations = database
                .context()
                .select()
                .from(EDUCATIONS)
                .orderBy(EDUCATIONS.RANK.asc())
                .fetch();
        cboEducation.removeAllItems();
        for (Record education : educations) {
            cboEducation.addItem(new ComboItem<>(
                    education.getValue(EDUCATIONS.DEGREE),
                    education.getValue(EDUCATIONS.ID))
            );
        }
        return educations;
    }

    public static Result<Record> populateCategory(JComboBox cboCategory) {
        Result<Record> categories = database
                .context()
                .select()
                .from(MEMBER_CATEGORIES)
                .orderBy(MEMBER_CATEGORIES.ID.asc())
                .fetch();
        cboCategory.removeAllItems();
        for (Record category : categories) {
            cboCategory.addItem(new ComboItem<>(
                    category.getValue(MEMBER_CATEGORIES.NAME),
                    category.getValue(MEMBER_CATEGORIES.ID))
            );
        }
        return categories;
    }

    public static Result<Record> populateMainSubject(JComboBox cboMainSubject) {
        Result<Record> mainSubjects = database
                .context()
                .select()
                .from(SUBJECTS)
                .orderBy(SUBJECTS.CODE.asc())
                .fetch();
        cboMainSubject.removeAllItems();
        for (Record mainSubject : mainSubjects) {
            cboMainSubject.addItem(new ComboItem<>(
                    mainSubject.getValue(SUBJECTS.CODE) + "-" + mainSubject.getValue(SUBJECTS.NAME),
                    mainSubject.getValue(SUBJECTS.ID)
            ));
        }
        return mainSubjects;
    }

    public static Result<Record> populateDivisionSubject(JComboBox cboDivisionSubject, Integer mainSubjectId) {
        Result<Record> mainSubjects = database
                .context()
                .select()
                .from(DIVISION_SUBJECTS)
                .where(DIVISION_SUBJECTS.MAIN_SUBJECT_ID.eq(mainSubjectId))
                .orderBy(DIVISION_SUBJECTS.CODE.asc())
                .fetch();
        cboDivisionSubject.removeAllItems();
        for (Record mainSubject : mainSubjects) {
            cboDivisionSubject.addItem(new ComboItem<>(
                    mainSubject.getValue(DIVISION_SUBJECTS.CODE) + "-" + mainSubject.getValue(DIVISION_SUBJECTS.NAME),
                    mainSubject.getValue(DIVISION_SUBJECTS.ID)
            ));
        }
        return mainSubjects;
    }

    public static Result<Record> populateAuthor(JComboBox cboAuthor) {
        Result<Record> authors = database
                .context()
                .select()
                .from(AUTHORS)
                .orderBy(AUTHORS.NAME.asc())
                .fetch();
        cboAuthor.removeAllItems();
        for (Record author : authors) {
            cboAuthor.addItem(new ComboItem<>(
                    author.getValue(AUTHORS.NAME),
                    author.getValue(AUTHORS.ID)
            ));
        }
        return authors;
    }

    public static Result<Record> populatePublisher(JComboBox cboPublisher) {
        Result<Record> publishers = database
                .context()
                .select()
                .from(PUBLISHERS)
                .orderBy(PUBLISHERS.NAME.asc())
                .fetch();
        cboPublisher.removeAllItems();
        for (Record publisher : publishers) {
            cboPublisher.addItem(new ComboItem<>(
                    publisher.getValue(PUBLISHERS.NAME),
                    publisher.getValue(PUBLISHERS.ID)
            ));
        }
        return publishers;
    }

    public static SelectOnConditionStep<Record> getBooks() {
        return database.context()
                .select()
                .from(BOOKS)
                .innerJoin(ITEMS)
                .on(BOOKS.ITEM_CODE.eq(ITEMS.ITEM_CODE))
                .innerJoin(DIVISION_SUBJECTS)
                .on(DIVISION_SUBJECTS.ID.eq(ITEMS.SUBJECT_ID))
                .innerJoin(SUBJECTS)
                .on(SUBJECTS.ID.eq(DIVISION_SUBJECTS.MAIN_SUBJECT_ID))
                .innerJoin(AUTHORS)
                .on(AUTHORS.ID.eq(BOOKS.AUTHOR_ID));
    }

    public static Result<Record> populateOverdueMember(JComboBox cboOverdueMember) {
        Result<Record> overdueMembers = database
                .context()
                .select()
                .from(MEMBERS)
                .innerJoin(HISTORIES)
                .on(HISTORIES.MEMBER_ID.eq(MEMBERS.ID))
                .where(HISTORIES.RETURN_DATE.isNull().and(HISTORIES.DUE_DATE.lt(Timestamp.valueOf(LocalDateTime.now()))))
                .fetch();
        cboOverdueMember.removeAllItems();
        for (Record overdueMember : overdueMembers) {
            cboOverdueMember.addItem(new ComboItem<>(
                    overdueMember.getValue(MEMBERS.ID) + " - " +overdueMember.getValue(MEMBERS.NAME),
                    overdueMember.getValue(MEMBERS.ID)
            ));
        }
        return overdueMembers;
    }

    public static void populatePreference(JComboBox cboPreference) {
        cboPreference.removeAllItems();
        for (String preference : new String[]{
                "Yearly",
                "Monthly"
        }) {
            cboPreference.addItem(preference);
        }
    }

    public static Result<Record1<Integer>> populateRevenueForOutstandingYear(JComboBox cboOverdueYear) {
        Result<Record1<Integer>> revenueYears = getYear(HISTORIES, HISTORIES.RETURN_DATE);
        return fillCombobox(cboOverdueYear, revenueYears);
    }

    public static Result<Record1<Integer>> populateRevenueForOutstandingMonth(JComboBox cboOverdueMonth, Integer year) {
        Result<Record1<Integer>> revenueMonths = getMonth(HISTORIES, HISTORIES.RETURN_DATE, year);
        return fillCombobox(cboOverdueMonth, revenueMonths);
    }

    public static Result<Record1<Integer>> populateRevenueYear(JComboBox cboOverdueYear) {
        Result<Record1<Integer>> revenueYears = getYear(HISTORIES, HISTORIES.RETURN_DATE, HISTORIES.CHARGED_AMOUNT);
        return fillCombobox(cboOverdueYear, revenueYears);
    }

    public static Result<Record1<Integer>> populateRevenueMonth(JComboBox cboOverdueMonth, Integer year) {
        Result<Record1<Integer>> revenueMonths = getMonth(HISTORIES, HISTORIES.RETURN_DATE, year, HISTORIES.CHARGED_AMOUNT);
        return fillCombobox(cboOverdueMonth, revenueMonths);
    }

    public static Result<Record1<Integer>> populateExpenseYear(JComboBox cboExpenseYear) {
        Result<Record1<Integer>> expenseYears = getYear(BUY_TRANSACTIONS, BUY_TRANSACTIONS.RECEIVING_DATE);
        return fillCombobox(cboExpenseYear, expenseYears);
    }

    public static Result<Record1<Integer>> populateExpenseMonth(JComboBox cboExpenseMonth, Integer year) {
        Result<Record1<Integer>> expenseMonths = getMonth(BUY_TRANSACTIONS, BUY_TRANSACTIONS.RECEIVING_DATE, year);
        return fillCombobox(cboExpenseMonth, expenseMonths);
    }

    private static Result<Record1<Integer>> getYear(TableImpl<?> table, TableField<?, Timestamp> dateField) {
        return getYear(table, dateField, null);
    }

    private static Result<Record1<Integer>> getYear(TableImpl<?> table, TableField<?, Timestamp> dateField, TableField<?, Short> amountField) {
        return database
                .context()
                .selectDistinct(DSL.year(dateField))
                .from(table)
                .where(dateField.isNotNull().and(null != amountField ? amountField.ne(Short.valueOf("0")) : DSL.trueCondition()))
                .orderBy(DSL.year(dateField).desc())
                .fetch();
    }
    private static Result<Record1<Integer>> getMonth(TableImpl<?> table, TableField<?, Timestamp> dateField, Integer year) {
        return getMonth(table, dateField, year, null);
    }

    private static Result<Record1<Integer>> getMonth(TableImpl<?> table, TableField<?, Timestamp> dateField, Integer year, TableField<?, Short> amountField) {
        return database
                .context()
                .selectDistinct(DSL.month(dateField))
                .from(table)
                .where(dateField.isNotNull().and(DSL.year(dateField).eq(year)).and(null != amountField ? amountField.ne(Short.valueOf("0")) : DSL.trueCondition()))
                .orderBy(DSL.month(dateField).desc())
                .fetch();
    }

    private static Result<Record1<Integer>> fillCombobox(JComboBox comboBox, Result<Record1<Integer>> data) {
        comboBox.removeAllItems();
        for (Record year : data) {
            comboBox.addItem(year.getValue(0));
        }
        return data;
    }

}
