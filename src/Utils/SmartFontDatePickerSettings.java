package Utils;

import com.github.lgooddatepicker.components.DatePickerSettings;

import java.awt.*;

public class SmartFontDatePickerSettings extends DatePickerSettings {

    public SmartFontDatePickerSettings(Font font) {
        setFontMonthAndYearMenuLabels(font);
        setFontCalendarDateLabels(font);
        setFontValidDate(font);
        setFontCalendarWeekdayLabels(font);
        setFontCalendarWeekNumberLabels(font);
        setFontClearLabel(font);
        setFontTodayLabel(font);
        setFontMonthAndYearNavigationButtons(font);
    }
}
