package Utils;

import org.jooq.Field;

public class ValueWrapper {
    private String key;
    private Field<String> value;

    public ValueWrapper(String key, Field<String> value) {
        this.key = key;
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public Field<String> getValue() {
        return value;
    }
}
