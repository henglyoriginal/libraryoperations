package Utils;

import java.sql.Time;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

public class DurationUtils {
    public static long durationBetween(LocalDateTime from, LocalDateTime to, ChronoUnit unit) {
        long difference = from.until(to, unit);
        difference = difference + ((from.until(to, ChronoUnit.MINUTES) > Duration.of(difference, unit).toMinutes()) ? 1 : 0) ;
        return difference;
    }
}
