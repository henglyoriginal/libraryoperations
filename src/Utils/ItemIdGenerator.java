package Utils;

import java.util.List;

public class ItemIdGenerator {

    public static String generate(List<String> ids, String prefix) {
        int size = ids.size();
        String itemCode = prefix + "-" + (0 == size ?  "00001" : "");
        if (0 != size) {
            String id = String.valueOf(Integer.valueOf(ids.get(0).split("\\-")[1]) + 1);
            while (id.length() < 5) {
                id = "0" + id;
            }
            itemCode += id;
        }
        return itemCode;
    }

}
