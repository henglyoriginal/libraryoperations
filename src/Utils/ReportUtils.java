package Utils;

import ro.nextreports.engine.Report;
import ro.nextreports.engine.util.LoadReportException;
import ro.nextreports.engine.util.ReportUtil;

import java.io.*;
import java.util.List;

public class ReportUtils {
    public static final String REPORT_HOME = "res\\report";

    public static Report getReport(String name) {
        String file = REPORT_HOME + File.separator + name + ".report";
        Report report = null;
        try {
            report = ReportUtil.loadReport(new FileInputStream(file));
            copyImages(report, REPORT_HOME, ".");
        } catch (LoadReportException | FileNotFoundException e) {
            e.printStackTrace();
            DialogUtils.showErrorMsg(null, e.getMessage());
        }
        return report;
    }

    public static void closeStream(Closeable stream) {
        if (stream == null) {
            return;
        }

        try {
            stream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void copyImages(Report report, String from, String to) {
        try {
            List<String> images = ReportUtil.getStaticImages(report);
            File destination = new File(to);
            for (String image : images) {
                File f = new File(from + File.separator + image);
                if (f.exists()) {
                    FileUtil.copyToDir(f, destination);
                }
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}
