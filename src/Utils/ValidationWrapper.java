package Utils;

import javax.swing.*;
import java.awt.*;

public class ValidationWrapper {
    private JComponent component;
    private Validator validator;
    private String errorMessage;

    @FunctionalInterface
    public interface Validator {
        boolean validate(JComponent component);
    }

    public ValidationWrapper(JComponent component, Validator validator) {
        this.component = component;
        this.validator = validator;
    }

    public ValidationWrapper(JComponent component, Validator validator, String errorMessage) {
        this.component = component;
        this.validator = validator;
        this.errorMessage = errorMessage;
    }

    /*package*/ boolean validate() {
        return validator.validate(component);
    }

    /*package*/ String getErrorMessage() {
        return errorMessage;
    }

    /*package*/ JComponent getComponent() {
        return component;
    }
}
