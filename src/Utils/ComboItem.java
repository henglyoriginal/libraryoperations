package Utils;

public class ComboItem<T1, T2>
{
    private T1 key;
    private T2 value;

    public ComboItem(T1 key, T2 value) {
        this.key = key;
        this.value = value;
    }

    @Override
    public String toString() {
        return key.toString();
    }

    public T1 getKey() {
        return key;
    }

    public T2 getValue() {
        return value;
    }
}
