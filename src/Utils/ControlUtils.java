package Utils;

import javax.swing.*;
import java.awt.*;

public class ControlUtils {

    public static boolean validateControl(Component parent, ValidationWrapper... validationWrappers) {
        for (ValidationWrapper item : validationWrappers) {
            if (!item.validate()) {
                String errorMessage = item.getErrorMessage();
                DialogUtils.showWarningMsg(parent, null == errorMessage ? "Invalid Input" : errorMessage);
                return false;
            } else {
                item.getComponent().grabFocus();
            }
        }
        return true;
    }

    public static void clearControls(JTextField... textFields) {
        for (JTextField textField : textFields) {
            textField.setText("");
        }
    }

    public static void toggleControl(boolean enabled, Component... components) {
        for (Component component : components) {
            component.setEnabled(enabled);
        }
    }

    @FunctionalInterface
    public interface Counter {
        Integer count();
    }

    public static class ControlWrapper {
        private JComponent component;
        private Counter counter;

        public ControlWrapper(JComponent component, Counter counter) {
            this.component = component;
            this.counter = counter;
        }
    }

    public static void controlEnabler(ControlWrapper... controlWrappers) {
        for (ControlWrapper controlWrapper : controlWrappers) {
            Integer count = controlWrapper.counter.count();
            controlWrapper.component.setEnabled(null != count && count > 0);
        }
    }
}
