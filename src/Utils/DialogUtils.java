package Utils;

import javax.swing.*;
import java.awt.*;

public class DialogUtils {

    public static void showWarningMsg(Component parent, String message) {
        JOptionPane.showMessageDialog(parent, message, "Warning", JOptionPane.WARNING_MESSAGE);
    }

    public static void showErrorMsg(Component parent, String message) {
        JOptionPane.showMessageDialog(parent, message, "Error", JOptionPane.ERROR_MESSAGE);
    }

    public static void showInfoMsg(Component parent, String message) {
        JOptionPane.showMessageDialog(parent, message, "Message", JOptionPane.INFORMATION_MESSAGE);
    }

    public static void yesNoDialog(Component parent, String message, String title, Runnable onYesAction, Runnable onNoAction) {
        if (JOptionPane.showConfirmDialog(
                parent,
                message,
                title,
                JOptionPane.YES_NO_OPTION,
                JOptionPane.QUESTION_MESSAGE) == JOptionPane.YES_OPTION
        ) {
            if (null != onYesAction) onYesAction.run();
        }
        else if (null != onNoAction) onNoAction.run();
    }

    public static String prompt(Component parent, String title) {
        JPasswordField passwordField = new JPasswordField();
        if (JOptionPane.showConfirmDialog(
                parent,
                passwordField,
                title,
                JOptionPane.OK_CANCEL_OPTION) == JOptionPane.OK_OPTION
        ) return new String(passwordField.getPassword());
        return null;
    }
}
