package Utils;

public interface Regex {
    String PHONE_NUMBER = "^855\\d{8,9}$";
    String NUMBER = "^\\d+$";
    String THREE_TO_FOUR_DIGITS = "^\\d{3,4}$";
    String ISBN = "^(?=(?:\\D*\\d){10}(?:(?:\\D*\\d){3})?$)[\\d-]+$";
    String MEMBER_ID = "^\\d{5}$";
    String ITEM_CODE = "^\\w{2}\\-\\d{5}\\.\\d{3}$";
}
