package Utils;

import com.github.lgooddatepicker.components.DatePicker;
import com.github.lgooddatepicker.components.DatePickerSettings;

import java.awt.*;

public class SmartDatePicker extends DatePicker {
    public SmartDatePicker(DatePickerSettings settings) {
        super(settings);
        getComponentDateTextField().setEnabled(false);
        getComponentDateTextField().setDisabledTextColor(new Color(128,128,128));

    }
}
