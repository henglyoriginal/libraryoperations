package Database;

/*
    Create a copy of this in the same directory and name it "DatabaseConfiguration" get it working
 */
public interface ExampleDatabaseConfiguration {
    String databaseName = "library";
    String databaseHost = "{db_host}";
    String databasePort = "3306";
    String databaseUsername = "{db_username}";
    String databasePassword = "{db_password}";
}
