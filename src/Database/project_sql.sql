/*

  *SQL SCRIPT TO CREATE EVERYTHING!
  *DO NOT CHANGE THIS IF UNAUTHORIZED

 */

DROP DATABASE IF EXISTS library;
CREATE DATABASE library;

USE library;

DELIMITER ;

SET GLOBAL log_bin_trust_function_creators = ON;
SET GLOBAL event_scheduler = ON;
SET GLOBAL time_zone = '+7:00';

CREATE TABLE authors (
   id INT AUTO_INCREMENT PRIMARY KEY,
   name VARCHAR(25) NOT NULL UNIQUE,
   sex CHAR(1) NOT NULL,
   address VARCHAR(100) NOT NULL
);

CREATE TABLE item_categories (
   id TINYINT AUTO_INCREMENT PRIMARY KEY,
   name VARCHAR(50) NOT NULL,
   prefix VARCHAR(2) NOT NULL,
   overdue_charge TINYINT NOT NULL,
   overdue_charge_unit VARCHAR(5) NOT NULL,
   borrowable_hours SMALLINT
);

CREATE TABLE member_categories (
   id TINYINT AUTO_INCREMENT PRIMARY KEY,
   name VARCHAR(25) NOT NULL,
   maximum_item TINYINT NOT NULL
);

CREATE TABLE borrow_detail_rules (
   member_category_id TINYINT NOT NULL,
   FOREIGN KEY (member_category_id) REFERENCES member_categories(id),
   item_category_id TINYINT NOT NULL,
   FOREIGN KEY (item_category_id) REFERENCES item_categories(id),
   special_borrow_hours SMALLINT,
   PRIMARY KEY (member_category_id, item_category_id)
);

CREATE TABLE subjects (
  id INT AUTO_INCREMENT PRIMARY KEY,
  code VARCHAR(5) NOT NULL,
  name VARCHAR(50) NOT NULL
);

CREATE TABLE division_subjects (
   id INT AUTO_INCREMENT PRIMARY KEY,
   code VARCHAR(5) NOT NULL,
   name VARCHAR(50) NOT NULL,
   main_subject_id INT NOT NULL,
   FOREIGN KEY (main_subject_id) REFERENCES subjects(id)
);

CREATE TABLE publishers (
  id INT AUTO_INCREMENT PRIMARY KEY,
  name VARCHAR(25) NOT NULL UNIQUE,
  address VARCHAR(100) NOT NULL,
  contact_number VARCHAR(30) NOT NULL
);

CREATE TABLE educations (
  id TINYINT AUTO_INCREMENT PRIMARY KEY,
  degree VARCHAR(30) NOT NULL,
  `rank` TINYINT NOT NULL
);

CREATE TABLE employees (
   id INT AUTO_INCREMENT PRIMARY KEY,
   name VARCHAR(25) NOT NULL,
   sex CHAR(1) NOT NULL,
   address VARCHAR(100) NOT NULL,
   hired_date DATE NOT NULL,
   contact_number VARCHAR(30) NOT NULL,
   username VARCHAR(20) NOT NULL UNIQUE,
   password VARCHAR(512) NOT NULL,
   is_admin TINYINT(1) NOT NULL DEFAULT 0,
   education_id TINYINT NOT NULL,
   FOREIGN KEY (education_id) REFERENCES educations(id)
);

CREATE TABLE buy_transactions (
  transaction_number INT AUTO_INCREMENT PRIMARY KEY,
  order_date DATETIME NOT NULL,
  receiving_date DATETIME,
  publisher_id INT NOT NULL,
  FOREIGN KEY (publisher_id) REFERENCES publishers(id),
  status VARCHAR(10) NOT NULL,
  order_employee_id INT NOT NULL,
  FOREIGN KEY (order_employee_id) REFERENCES employees(id),
  receive_employee_id INT,
  FOREIGN KEY (receive_employee_id) REFERENCES employees(id),
  cancel_employee_id INT,
  FOREIGN KEY (cancel_employee_id) REFERENCES employees(id)

);

CREATE TABLE items (
   item_code VARCHAR(8) NOT NULL PRIMARY KEY,
   title VARCHAR(50) NOT NULL,
   owned_copies SMALLINT NOT NULL ,
   borrowed_copies SMALLINT NOT NULL,
   category_id TINYINT NOT NULL,
   FOREIGN KEY (category_id) REFERENCES item_categories(id),
   subject_id INT NOT NULL,
   FOREIGN KEY (subject_id) REFERENCES division_subjects(id)
);

CREATE TABLE books (
   item_code VARCHAR(8) NOT NULL PRIMARY KEY,
   FOREIGN KEY (item_code) REFERENCES items(item_code) ON DELETE CASCADE ON UPDATE CASCADE,
   isbn VARCHAR(20) UNIQUE,
   publication_year SMALLINT(4),
   keyword VARCHAR(30) NOT NULL,
   author_id INT NOT NULL,
   FOREIGN KEY (author_id) REFERENCES authors(id)
);

CREATE TABLE journals (
  item_code VARCHAR(8) NOT NULL PRIMARY KEY,
  FOREIGN KEY (item_code) REFERENCES items(item_code) ON DELETE CASCADE ON UPDATE CASCADE,
  yearly_public_frequency SMALLINT,
  affiliated_research_society VARCHAR(25)
);

CREATE TABLE dvds (
  item_code VARCHAR(8) NOT NULL PRIMARY KEY,
  FOREIGN KEY (item_code) REFERENCES items(item_code) ON DELETE CASCADE ON UPDATE CASCADE,
  comments VARCHAR(100),
  producer VARCHAR(25)
);

CREATE TABLE conference_proceedings (
  item_code VARCHAR(8) NOT NULL PRIMARY KEY,
  FOREIGN KEY (item_code) REFERENCES items(item_code) ON DELETE CASCADE ON UPDATE CASCADE,
  conference_date DATE
);

CREATE TABLE item_details (
  id INT AUTO_INCREMENT PRIMARY KEY,
  item_code VARCHAR(8) NOT NULL,
  FOREIGN KEY (item_code) REFERENCES items(item_code) ON DELETE CASCADE ON UPDATE CASCADE,
  item_sub_code VARCHAR(3) NOT NULL,
  `condition` VARCHAR(10) NOT NULL,
  is_on_shelf TINYINT(1) NOT NULL,
  is_on_loan TINYINT(1) NOT NULL
);

CREATE TABLE members (
   id INT AUTO_INCREMENT PRIMARY KEY,
   name VARCHAR(25) NOT NULL,
   sex CHAR(1) NOT NULL,
   address VARCHAR(100),
   category_id TINYINT NOT NULL,
   FOREIGN KEY (category_id) REFERENCES member_categories(id) ,
   is_active TINYINT(1) NOT NULL,
   registration_date DATE NOT NULL,
   expire_date DATE
);

CREATE TABLE histories (
   id INT AUTO_INCREMENT PRIMARY KEY,
   member_id INT NOT NULL,
   FOREIGN KEY (member_id) REFERENCES members(id),
   item_detail_id INT NOT NULL,
   FOREIGN KEY (item_detail_id) REFERENCES item_details(id),
   borrowed_date DATETIME NOT NULL,
   due_date DATETIME NOT NULL,
   return_date DATETIME,
   charged_amount SMALLINT,
   loan_employee_id INT NOT NULL,
   accept_employee_id INT,
   FOREIGN KEY (loan_employee_id) REFERENCES employees(id),
   FOREIGN KEY (accept_employee_id) REFERENCES employees(id)
);

CREATE TABLE transaction_details (
   transaction_number INT NOT NULL,
   FOREIGN KEY (transaction_number) REFERENCES buy_transactions(transaction_number) ON DELETE CASCADE ON UPDATE CASCADE,
   item_code VARCHAR(8) NOT NULL,
   FOREIGN KEY (item_code) REFERENCES items(item_code) ON DELETE CASCADE ON UPDATE CASCADE,
   quantity SMALLINT NOT NULL,
   item_price DOUBLE(5, 2) NOT NULL,
   total_amount DOUBLE(5, 2) NOT NULL,
   PRIMARY KEY (transaction_number, item_code)
);

INSERT INTO educations (degree, `rank`)
VALUES ('Undergraduate', 0),
       ('High School Graduate', 1),
       ('Bachelor Degree', 2),
       ('Master Degree', 3),
       ('Doctoral Degree', 4),
       ('Professional Degree', 5);

INSERT INTO employees (name, sex, address, hired_date, contact_number, username, password, is_admin, education_id)
VALUES ('Administrator', 'M', 'Earth', '1900-01-01', '015220598', 'admin', '37bd45d638c2d11c49c641d2e9c4f49f406caf3ee282743e0c800aa1ed68e2ee', 1, 5),
       ('Tester', 'M', 'Earth', '1900-01-01', '015220599', 'tester', 'ef797c8118f02dfb649607dd5d3f8c7623048c9c063d532cc95c5ed7a898a64f', 0, 4);

INSERT INTO member_categories (name, maximum_item)
VALUES ('Faculty', 20),
       ('Graduate Student', 15),
       ('Undergraduate Student', 10),
       ('Visiting Scholar', 5),
       ('Part-time Student', 5),
       ('Staff', 3);

INSERT INTO item_categories (name, prefix, overdue_charge, overdue_charge_unit, borrowable_hours)
VALUES ('Books', 'BK', 1, 'Day', NULL),
       ('Journals', 'JN', 5, 'Day', 48),
       ('Conference Proceedings', 'CP', 5, 'Day', 48),
       ('DVDs', 'DV', 1, 'Hour', 3),
       ('Reference Textbooks', 'RT', 1, 'Hour', NULL);

INSERT INTO borrow_detail_rules(member_category_id, item_category_id, special_borrow_hours)
VALUES (1, 1, 504),
       (1, 2, NULL),
       (1, 3, NULL),
       (2, 1, 504),
       (2, 2, NULL),
       (2, 5, NULL),
       (2, 4, NULL),
       (3, 1, 336),
       (3, 5, NULL),
       (3, 4, NULL),
       (4, 1, 336),
       (4, 2, NULL),
       (4, 3, NULL),
       (5, 1, 336),
       (5, 4, NULL),
       (6, 1, 168);

INSERT INTO subjects (code, name)
VALUES ('001', 'Information Technology'),
       ('002', 'Business'),
       ('003', 'History'),
       ('004', 'Politics'),
       ('005', 'Fiction'),
       ('006', 'Song');

INSERT INTO division_subjects (code, name, main_subject_id)
VALUES ('01', 'Software Development', 1),
       ('02', 'Graphic Design', 1),
       ('03', 'Network Administration', 1),
       ('01', 'Marketing', 2),
       ('02', 'Accountant', 2),
       ('03', 'International Business', 2),
       ('01', 'War', 3),
       ('02', 'Ancient', 3),
       ('03', 'Proto History', 3),
       ('01', 'Revolutionary', 4),
       ('02', 'Democracy', 4),
       ('03', 'Communism', 4),
       ('01', 'Fantasy', 5),
       ('02', 'Anime', 5),
       ('01', 'Pop', 6),
       ('02', 'Rock', 6),
       ('03', 'Metal', 6),
       ('04', 'Jazz', 6);


INSERT INTO authors (name, sex, address)
VALUES ('J. R. R. Tolkien', 'M', 'United Kingdom');

INSERT INTO publishers (name, address, contact_number)
VALUES ('Hengly', 'Tuol Kork, Phnom Penh', '85515220598'),
       ('Houghton Mifflin', 'Boston, MA, USA', '85515220598');

INSERT INTO items (item_code, title, owned_copies, borrowed_copies, category_id, subject_id)
VALUES ('BK-00001', 'The Fellowship of the Ring', 0, 0, 1, 13),
       ('BK-00002', 'The Two Towers', 0, 0, 1, 13),
       ('BK-00003', 'The Return of the King', 0, 0, 1, 13);

INSERT INTO books (item_code, isbn, publication_year, keyword, author_id)
VALUES  ('BK-00001', '978-0345339706', 1986, 'lord of the ring fellowship', 1),
        ('BK-00002', '978-0618002238', 1982, 'lord of the ring two towers', 1),
        ('BK-00003', '978-0345339737', 1986, 'lord of the ring return king', 1);

INSERT INTO members (name, sex, address, category_id, is_active, registration_date, expire_date)
VALUES ('Hengly Bunhour', 'M', 'St 186z, Teuk Laak 3, Toul Kork, Phnom Penh', 3, 1, '2019-03-22', '2019-09-22'),
       ('Disabled guy', 'M', 'Tuol Kork, Phnom Penh', 3, 0, '2018-09-22', '2019-03-22');


DELIMITER $$

CREATE FUNCTION authenticate(username VARCHAR(20), password VARCHAR(512)) RETURNS INT
BEGIN
  DECLARE authId INT DEFAULT -1;
  SELECT employees.id INTO authId FROM employees
  WHERE employees.username = username AND employees.password = password;
  RETURN authId;
END

$$

-- Start Employee --

CREATE PROCEDURE sign_me_up(
  name VARCHAR(25),
  sex CHAR(1),
  address VARCHAR(100),
  hired_date DATE,
  contact_number VARCHAR(30),
  username VARCHAR(20),
  password VARCHAR(512),
  education_id TINYINT
)
BEGIN
  INSERT INTO employees (name, sex, address, hired_date, contact_number, username, password, education_id)
  VALUES (name, sex, address, hired_date, contact_number, username, password, education_id);
END

$$

CREATE PROCEDURE update_employee_without_password(
  id INT,
  name VARCHAR(25),
  sex CHAR(1),
  address VARCHAR(100),
  hired_date DATE,
  contact_number VARCHAR(30),
  username VARCHAR(20),
  education_id TINYINT
)
BEGIN
UPDATE employees
SET
  employees.name = name,
  employees.sex = sex,
  employees.address = address,
  employees.hired_date = hired_date,
  employees.contact_number = contact_number,
  employees.username = username,
  employees.education_id = education_id
WHERE
    employees.id = id;
END

$$

CREATE PROCEDURE update_employee(
  id INT,
  name VARCHAR(25),
  sex CHAR(1),
  address VARCHAR(100),
  hired_date DATE,
  contact_number VARCHAR(30),
  username VARCHAR(20),
  password VARCHAR(512),
  education_id TINYINT
)
BEGIN

  CALL update_employee_without_password(
  id, name, sex, address, hired_date, contact_number, username, education_id
  );

  UPDATE employees
  SET
    employees.password = password
  WHERE
      employees.id = id;
END

$$

CREATE PROCEDURE remove_employee(
  id INT
)
BEGIN
  DELETE FROM employees WHERE employees.id = id;
END

-- End Employee --

$$

-- Start Member --

CREATE PROCEDURE add_member(
  name VARCHAR(25),
  sex CHAR(1),
  address VARCHAR(100),
  is_active TINYINT,
  registration_date DATE,
  expire_date DATE,
  category_id TINYINT
)
BEGIN
  INSERT INTO members (name, sex, address, is_active, registration_date, expire_date, category_id)
  VALUES (name, sex, address, is_active, registration_date, expire_date, category_id);
END

$$

CREATE PROCEDURE update_member(
  id INT,
  name VARCHAR(25),
  sex CHAR(1),
  address VARCHAR(100),
  category_id TINYINT
)
BEGIN
  UPDATE members
  SET
    members.name = name,
    members.sex = sex,
    members.address = address,
    members.category_id = category_id
  WHERE
      members.id = id;
END

$$

CREATE PROCEDURE remove_member(
  id INT
)
BEGIN
  DELETE FROM members WHERE members.id = id;
END

-- End Member --

$$

-- Start Book --

CREATE PROCEDURE add_book (
  title VARCHAR(50),
  item_code VARCHAR(8),
  subject_id INT,
  isbn VARCHAR(20),
  publication_year SMALLINT(4),
  keyword VARCHAR(30),
  author_id INT
)
BEGIN
  INSERT INTO items (title, item_code, owned_copies, borrowed_copies, category_id, subject_id)
  VALUES (title, item_code, 0, 0, 1, subject_id);

  INSERT INTO books (item_code, isbn, publication_year, keyword, author_id)
  VALUES (item_code, isbn, publication_year, keyword, author_id);
END

$$

CREATE PROCEDURE update_book (
  item_code VARCHAR(8),
  title VARCHAR(50),
  subject_id INT,
  isbn VARCHAR(20),
  publication_year SMALLINT(4),
  keyword VARCHAR(30),
  author_id INT
)
BEGIN
  UPDATE items
  SET
    items.title = title,
    items.subject_id = subject_id
  WHERE items.item_code = item_code;

  UPDATE books
  SET
    books.isbn = isbn,
    books.publication_year = publication_year,
    books.keyword = keyword,
    books.author_id = author_id
  WHERE books.item_code = item_code;
END

-- End Book --

$$

-- Start Journal --

CREATE PROCEDURE add_journal (
  title VARCHAR(50),
  item_code VARCHAR(8),
  subject_id INT,
  yearly_public_frequency SMALLINT,
  affiliated_research_society VARCHAR(25)
)
BEGIN
  INSERT INTO items (title, item_code, owned_copies, borrowed_copies, category_id, subject_id)
  VALUES (title, item_code, 0, 0, 2, subject_id);

  INSERT INTO journals (item_code, yearly_public_frequency, affiliated_research_society)
  VALUES (item_code, yearly_public_frequency, affiliated_research_society);
END

$$

CREATE PROCEDURE update_journal (
  item_code VARCHAR(8),
  title VARCHAR(50),
  subject_id INT,
  yearly_public_frequency SMALLINT,
  affiliated_research_society VARCHAR(25)
)
BEGIN
  UPDATE items
  SET
    items.title = title,
    items.subject_id = subject_id
  WHERE items.item_code = item_code;

  UPDATE journals
  SET
    journals.yearly_public_frequency = yearly_public_frequency,
    journals.affiliated_research_society = affiliated_research_society
  WHERE journals.item_code = item_code;
END

-- End Journal --

$$

-- Start Conference Proceeding --

CREATE PROCEDURE add_conference_proceeding (
  title VARCHAR(50),
  item_code VARCHAR(8),
  subject_id INT,
  conference_date DATE
)
BEGIN
  INSERT INTO items (title, item_code, owned_copies, borrowed_copies, category_id, subject_id)
  VALUES (title, item_code, 0, 0, 3, subject_id);

  INSERT INTO conference_proceedings (item_code, conference_date)
  VALUES (item_code, conference_date);

END

$$

CREATE PROCEDURE update_conference_proceeding (
  item_code VARCHAR(8),
  title VARCHAR(50),
  subject_id INT,
  conference_date DATE
)
BEGIN
  UPDATE items
  SET
    items.title = title,
    items.subject_id = subject_id
  WHERE items.item_code = item_code;

  UPDATE conference_proceedings
  SET
    conference_proceedings.conference_date = conference_date
  WHERE conference_proceedings.item_code = item_code;
END

-- End Conference Proceeding --

$$

-- Start DVD --

CREATE PROCEDURE add_dvd (
  title VARCHAR(50),
  item_code VARCHAR(8),
  subject_id INT,
  comments VARCHAR(100),
  producer VARCHAR(25)
)
BEGIN
  INSERT INTO items (title, item_code, owned_copies, borrowed_copies, category_id, subject_id)
  VALUES (title, item_code, 0, 0, 4, subject_id);

  INSERT INTO dvds (item_code, comments, producer)
  VALUES (item_code, comments, producer);

END

$$

CREATE PROCEDURE update_dvd (
  item_code VARCHAR(8),
  title VARCHAR(50),
  subject_id INT,
  comments VARCHAR(100),
  producer VARCHAR(25)
)
BEGIN
  UPDATE items
  SET
    items.title = title,
    items.subject_id = subject_id
  WHERE items.item_code = item_code;

  UPDATE dvds
  SET
    dvds.producer = producer,
    dvds.comments = comments
  WHERE dvds.item_code = item_code;
END

-- End DVD --

$$

-- Start Reference Textbook --

CREATE PROCEDURE add_reference_textbook (
  title VARCHAR(50),
  item_code VARCHAR(8),
  subject_id INT
)
BEGIN
  INSERT INTO items (title, item_code, owned_copies, borrowed_copies, category_id, subject_id)
  VALUES (title, item_code, 0, 0, 5, subject_id);
END

$$

CREATE PROCEDURE update_reference_textbook (
  item_code VARCHAR(8),
  title VARCHAR(50),
  subject_id INT
  )
BEGIN
  UPDATE items
  SET
    items.title = title,
    items.subject_id = subject_id
  WHERE items.item_code = item_code;
END

-- End Reference Textbook --

$$

-- Start Item --

CREATE PROCEDURE delete_item (
  item_code VARCHAR(8)
)
BEGIN
  DELETE FROM items WHERE items.item_code = item_code;
END

-- End Item --

$$

-- Start Item Detail --

CREATE PROCEDURE update_item_count (
  item_code VARCHAR(8)
)
BEGIN
  DECLARE owned SMALLINT DEFAULT 0;
  DECLARE loan SMALLINT DEFAULT 0;

  SELECT COUNT(*) INTO owned
  FROM item_details
  WHERE item_details.item_code = item_code;

  SELECT COUNT(*) INTO loan
  FROM item_details
  WHERE item_details.is_on_loan = 1 AND item_details.item_code = item_code;

  UPDATE items
  SET
    items.owned_copies = owned,
    items.borrowed_copies = loan
  WHERE items.item_code = item_code;
END

$$

CREATE PROCEDURE add_item_detail (
  item_code VARCHAR(8),
  item_sub_code VARCHAR(3),
  `condition` VARCHAR(10)
)
BEGIN

  INSERT INTO item_details (item_code, item_sub_code, `condition`, is_on_shelf, is_on_loan)
  VALUES (item_code, item_sub_code, `condition`, 1, 0);

  CALL update_item_count(item_code);
END

$$

CREATE PROCEDURE update_item_detail (
  item_code VARCHAR(8),
  item_sub_code VARCHAR(3),
  `condition` VARCHAR(10)
)
BEGIN
  UPDATE item_details
  SET
    item_details.condition = `condition`
  WHERE item_details.item_code = item_code AND item_details.item_sub_code = item_sub_code;

  CALL update_item_count(item_code);
END

$$

CREATE PROCEDURE delete_item_detail (
  item_code VARCHAR(8),
  item_sub_code VARCHAR(3)
)
BEGIN
  DELETE FROM item_details WHERE item_details.item_code = item_code AND item_details.item_sub_code = item_sub_code;
  CALL update_item_count(item_code);
END

-- End Item Detail --

$$

-- Start Author --

CREATE PROCEDURE add_author (
  name VARCHAR(25),
  sex CHAR(1),
  address VARCHAR(100)
)
BEGIN
  INSERT INTO authors (name, sex, address)
  VALUES (name, sex, address);
END

$$

CREATE PROCEDURE update_author (
  id INT,
  name VARCHAR(25),
  sex CHAR(1),
  address VARCHAR(100)
)
BEGIN
  UPDATE authors
  SET
    authors.name = name,
    authors.sex = sex,
    authors.address = address
  WHERE authors.id = id;
END

$$

CREATE PROCEDURE delete_author (
  id INT
)
BEGIN
  DELETE FROM authors WHERE authors.id = id;
END

-- End Author --

$$

CREATE FUNCTION is_username_exists(username VARCHAR(20)) RETURNS INT
BEGIN
  DECLARE is_exists INT DEFAULT -1;
  SELECT employees.id INTO is_exists FROM employees WHERE employees.username = username AND employees.username <>'';
  RETURN is_exists;
END

$$

-- Start publisher --

CREATE PROCEDURE add_new_publisher (
  name VARCHAR(25),
  address VARCHAR(100),
  contact_number VARCHAR(30)
)
BEGIN
  INSERT INTO publishers (name, address, contact_number)
  VALUES (name, address, contact_number);
END

$$

CREATE PROCEDURE update_publisher(
  id INT,
  name VARCHAR(25),
  address VARCHAR(100),
  contact_number VARCHAR(30)
)
BEGIN
  UPDATE publishers
  SET
    publishers.name = name,
    publishers.address = address,
    publishers.contact_number = contact_number
  WHERE
      publishers.id = id;
END

$$

CREATE PROCEDURE remove_publisher(
  id INT
)
BEGIN
  DELETE FROM publishers WHERE publishers.id = id;
END

-- End publisher --

$$

CREATE FUNCTION add_order(
  publisher_id INT,
  order_employee_id INT
) RETURNS INT
BEGIN
  INSERT INTO buy_transactions (order_date, receiving_date, publisher_id, status, order_employee_id, receive_employee_id, cancel_employee_id)
  VALUES (localtime, NULL, publisher_id, 'Shipping', order_employee_id, NULL, NULL);

  RETURN LAST_INSERT_ID();
END

$$

CREATE PROCEDURE add_order_detail(
  transaction_number INT,
  item_code VARCHAR(8),
  quantity SMALLINT,
  item_price DOUBLE(5, 2)
)
BEGIN
  INSERT INTO transaction_details (transaction_number, item_code, quantity, item_price, total_amount)
  VALUES (transaction_number, item_code, quantity, item_price, quantity*item_price);
END

$$

CREATE PROCEDURE accept_order(
  transaction_number INT,
  receive_employee_id INT
)
BEGIN
  UPDATE buy_transactions
  SET receiving_date = localtime,
      receive_employee_id = receive_employee_id,
      status = 'Accepted'
  WHERE buy_transactions.transaction_number = transaction_number;
END

$$

CREATE PROCEDURE cancel_order(
  transaction_number INT,
  cancel_employee_id INT
)
BEGIN
  UPDATE buy_transactions
  SET cancel_employee_id = cancel_employee_id,
      status = 'Cancelled'
  WHERE buy_transactions.transaction_number = transaction_number;
END

$$

CREATE FUNCTION on_going_loan_amount(
  member_id INT
)
RETURNS TINYINT(4)
BEGIN
  DECLARE on_going_loan TINYINT(4);

  SELECT count(*) INTO on_going_loan FROM members m
  INNER JOIN histories h ON m.id = h.member_id
  WHERE m.id = member_id AND h.return_date IS NULL;

  RETURN on_going_loan;
END

$$

CREATE FUNCTION borrowable_amount(
  member_id INT
) RETURNS TINYINT(4)
BEGIN
  DECLARE max_item TINYINT(4);
  DECLARE on_going_loan TINYINT(4);

  SELECT mc.maximum_item INTO max_item FROM members m
  INNER JOIN member_categories mc ON m.category_id = mc.id
  WHERE m.id = member_id;

  SELECT on_going_loan_amount(member_id) INTO on_going_loan;

RETURN (max_item - on_going_loan);

END

$$

CREATE FUNCTION is_member_exists(
member_id INT
) RETURNS TINYINT(1)
BEGIN

DECLARE is_exist TINYINT(1);
SELECT members.id INTO is_exist FROM members
WHERE members.id = member_id;

RETURN is_exist;

END

$$

CREATE FUNCTION is_member_active(
  member_id INT
) RETURNS TINYINT(1)
BEGIN

  DECLARE is_active TINYINT(1);
  SELECT members.is_active INTO is_active FROM members
  WHERE members.id = member_id;

  RETURN is_active;

END

$$

CREATE PROCEDURE activate_member(
  member_id INT,
  expire_date DATE
)
BEGIN
  UPDATE members
  SET expire_date = expire_date,
      is_active = 1
  WHERE members.id = member_id;
END

$$

CREATE PROCEDURE add_loan(
  member_id INT,
  item_detail_id INT,
  employee_id INT,
  due_date DATETIME
)
BEGIN

  DECLARE item_code VARCHAR(8);

  INSERT INTO histories (member_id, item_detail_id, borrowed_date, due_date, return_date, charged_amount, loan_employee_id)
  VALUES (member_id, item_detail_id, localtimestamp, due_date, NULL, NULL, employee_id);

  UPDATE item_details
  SET item_details.is_on_loan = 1,
      item_details.is_on_shelf = 0
  WHERE item_details.id = item_detail_id;

  SELECT item_details.item_code INTO item_code FROM item_details
  WHERE item_details.id = item_detail_id;

  CALL update_item_count(item_code);

END

$$

CREATE PROCEDURE accept_return(
  history_id INT,
  item_detail_id INT,
  employee_id INT,
  charged_amount SMALLINT
)
BEGIN

  DECLARE item_code VARCHAR(8);

  UPDATE histories
  SET return_date = localtimestamp,
      charged_amount = charged_amount,
      accept_employee_id = employee_id
  WHERE histories.id = history_id;

  UPDATE item_details
  SET item_details.is_on_loan = 0,
      item_details.is_on_shelf = 1
  WHERE item_details.id = item_detail_id;

  SELECT item_details.item_code INTO item_code FROM item_details
  WHERE item_details.id = item_detail_id;

  CALL update_item_count(item_code);

END

$$

CREATE FUNCTION count_poor_items()
RETURNS INT
BEGIN
  DECLARE poor_item_amount INT;

  SELECT COUNT(*) INTO poor_item_amount
  FROM
    items i2,
    item_details i1
  WHERE
      i1.item_code = i2.item_code AND
      i1.condition = 'Poor';

  RETURN poor_item_amount;
END

$$

CREATE FUNCTION count_overdue_items()
RETURNS INT
BEGIN
  DECLARE overdue_item_amount INT;

  SELECT COUNT(*) INTO overdue_item_amount
  FROM
    items i2,
    histories h1,
    item_categories i3,
    members m1,
    member_categories m2,
    item_details i1
  WHERE
      m1.category_id = m2.id AND
      h1.member_id = m1.id AND
      h1.item_detail_id = i1.id AND
      i1.item_code = i2.item_code AND
      i2.category_id = i3.id AND
      h1.return_date IS NULL  AND
      h1.due_date < localtime;

  RETURN overdue_item_amount;
END

$$

CREATE FUNCTION count_outstanding_members()
RETURNS INT
BEGIN
  DECLARE outstanding_member_amount INT;

  SELECT COUNT(*) INTO outstanding_member_amount
  FROM
    members m1,
    histories h1
  WHERE
        h1.member_id = m1.id AND
        h1.return_date IS NOT NULL
  GROUP BY
    m1.name,
    m1.sex,
    m1.id;

  RETURN outstanding_member_amount;
END

$$

CREATE FUNCTION count_expenses()
RETURNS INT
BEGIN
  DECLARE expenses_count INT;
  SELECT COUNT(*) INTO expenses_count
  FROM
    publishers p1,
    transaction_details t1,
    buy_transactions b1
  WHERE
        b1.publisher_id = p1.id AND
        b1.transaction_number = t1.transaction_number AND
        b1.receiving_date IS NOT NULL
  GROUP BY
    p1.id,
    p1.name;
  RETURN expenses_count;
END

$$

CREATE FUNCTION count_revenues()
RETURNS INT
BEGIN
  DECLARE revenues_count INT;
  SELECT COUNT(*) INTO revenues_count
  FROM
    items i2,
    histories h1,
    item_details i1
  WHERE
        h1.item_detail_id = i1.id AND
        i1.item_code = i2.item_code AND
        h1.return_date IS NOT NULL AND
        h1.charged_amount > 0
  GROUP BY
    i2.item_code,
    i2.title;
  RETURN revenues_count;
END

$$

CREATE EVENT daily_check_member_expire_date
ON SCHEDULE
EVERY 1 MINUTE
STARTS (localtime + INTERVAL 30 SECOND)
COMMENT 'If the member expire date is greater than localtime, change status to disabled.'
DO
BEGIN
  UPDATE members
  SET
    is_active = 0
  WHERE members.expire_date < date(localtime);
END

$$

DELIMITER ;

SHOW COUNT(*) ERRORS;
