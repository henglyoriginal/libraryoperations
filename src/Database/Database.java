package Database;

import org.jooq.DSLContext;
import org.jooq.SQLDialect;
import org.jooq.impl.DSL;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Database {
    private DSLContext context;
    private Connection connection;

    /* Change properties in DatabaseConfiguration */
    public Database() {
        try {
            connection = DriverManager.getConnection(
                    "jdbc:mysql://" + DatabaseConfiguration.databaseHost + ":" + DatabaseConfiguration.databasePort + "/" + DatabaseConfiguration.databaseName + "?AllowPublicKeyRetrieval=true&useSSL=false&useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=Asia/Phnom_Penh",
                    DatabaseConfiguration.databaseUsername,
                    DatabaseConfiguration.databasePassword
            );
            context = DSL.using(connection, SQLDialect.MYSQL);
        } catch(SQLException ex) {
            ex.printStackTrace();
        }
    }

    public DSLContext context() {
        return context;
    }

    public Connection getConnection() {
        return connection;
    }
}
