package Admin.LoanReturnTransaction;

import Admin.Application.Main;
import Database.generated.Routines;
import Utils.*;
import org.jooq.Configuration;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.Date;
import java.time.LocalDate;
import java.time.ZoneId;

import static Admin.Application.Main.database;

public class LoanReturnTransactionForm extends SmartJFrame {
    private JPanel view;
    private JTextField txtMemberId;
    private JButton btnCheck;
    private JLabel btnBack;
    private JLabel lblLoan;
    private JLabel lblReturn;
    private JLabel lblActivate;
    private LocalDate now = LocalDate.now(ZoneId.of("GMT+7"));

    public LoanReturnTransactionForm(Component parent) {
        super(parent);
        initializeForm();
        setTitle("Loan/Return Transaction");
        setContentPane(view);
        setSize(650,400);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setLocationRelativeTo(parent);
        setResizable(false);
        setVisible(true);
    }


    @Override
    public void initializeForm() {

        txtMemberId.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                action();
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                action();
            }

            @Override
            public void changedUpdate(DocumentEvent e) {

            }

            private void action() {
                lblActivate.setEnabled(false);
                lblLoan.setEnabled(false);
                lblReturn.setEnabled(false);
                btnCheck.setEnabled(!"".equals(txtMemberId.getText()));
            }
        });

        btnCheck.addActionListener(e -> {
            if (txtMemberId.getText().matches(Regex.MEMBER_ID)) {
                if (null != Routines.isMemberExists(database.context().configuration(), getMemberId())) {
                    refreshButtons();
                } else {
                    DialogUtils.showWarningMsg(LoanReturnTransactionForm.this, "Member '" + txtMemberId.getText() + "' is not exists");
                }
            } else {
                DialogUtils.showWarningMsg(LoanReturnTransactionForm.this, "Invalid Member ID's format (#####)");
            }
        });

        lblLoan.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (lblLoan.isEnabled()) FormSwitching.switchForm(LoanReturnTransactionForm.this, LoanTransactionForm.class, txtMemberId.getText());
            }
        });

        lblReturn.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (lblReturn.isEnabled()) FormSwitching.switchForm(LoanReturnTransactionForm.this, ReturnTransactionForm.class, txtMemberId.getText());
            }
        });

        lblActivate.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (lblActivate.isEnabled() && AuthenticateUtils.isAuthenticated(LoanReturnTransactionForm.this, Main.activeUsername, "your")) {
                    Routines.activateMember(
                            database.context().configuration(),
                            getMemberId(),
                            Date.valueOf(now.plusMonths(6))
                    );
                    DialogUtils.showInfoMsg(LoanReturnTransactionForm.this, "Successfully extended 6 months!");
                    btnCheck.doClick();
                }
            }
        });

        addBackListener(btnBack);
    }

    private Integer getMemberId() {
        return Integer.valueOf(txtMemberId.getText());
    }

    private void refreshButtons() {
        if (!"".equals(txtMemberId.getText())) {
            Integer memId = getMemberId();
            Configuration configuration = database.context().configuration();
            Byte isMemberActive = Routines.isMemberActive(configuration, memId);
            lblReturn.setEnabled(Routines.onGoingLoanAmount(configuration, memId) > 0);
            lblLoan.setEnabled(Routines.borrowableAmount(configuration, memId) > 0 && isMemberActive == 1);
            lblActivate.setEnabled(isMemberActive != 1);
        }
    }

    @Override
    public void setVisible(boolean b) {
        super.setVisible(b);
        if (b) refreshButtons();
    }
}
