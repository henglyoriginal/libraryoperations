package Admin.LoanReturnTransaction;

import Admin.Application.Main;
import Database.generated.Routines;
import Utils.*;
import org.jooq.Record;
import org.jooq.Record3;
import org.jooq.Result;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;
import java.awt.*;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.*;
import java.util.List;

import static Admin.Application.Main.database;
import static Database.generated.tables.BorrowDetailRules.BORROW_DETAIL_RULES;
import static Database.generated.tables.ItemCategories.ITEM_CATEGORIES;
import static Database.generated.tables.ItemDetails.ITEM_DETAILS;
import static Database.generated.tables.Items.ITEMS;
import static Database.generated.tables.Members.MEMBERS;

public class LoanTransactionForm extends SmartJFrame {
    private JPanel view;
    private JTextField txtItemCode;
    private JTable tblItem;
    private JButton btnAdd;
    private JButton btnLoan;
    private JButton btnBack;
    private Integer memberId;
    private Byte borrowableAmount;
    private DefaultTableModel tbmItem;
    private HashSet<Record> items = new HashSet<>();
    private Result<Record3<String, Short, Short>> borrowableDurations;
    private Map<String, Short> borrowableAmountMapper = new HashMap<>();

    public LoanTransactionForm(Component parent, String memberId) {
        super(parent);
        this.memberId = Integer.valueOf(memberId);
        initializeForm();
        setTitle("Loan Transaction");
        setContentPane(view);
        setSize(700,450);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setLocationRelativeTo(parent);
        setResizable(false);
        setVisible(true);
    }

    @Override
    public void initializeForm() {
        borrowableAmount = Routines.borrowableAmount(database.context().configuration(), memberId);
        btnAdd.setText("Add (" + borrowableAmount + ")");
        borrowableDurations = database.context()
                .select(ITEM_CATEGORIES.PREFIX, ITEM_CATEGORIES.BORROWABLE_HOURS, BORROW_DETAIL_RULES.SPECIAL_BORROW_HOURS)
                .from(MEMBERS)
                .innerJoin(BORROW_DETAIL_RULES).on(MEMBERS.CATEGORY_ID.eq(BORROW_DETAIL_RULES.MEMBER_CATEGORY_ID))
                .innerJoin(ITEM_CATEGORIES).on(BORROW_DETAIL_RULES.ITEM_CATEGORY_ID.eq(ITEM_CATEGORIES.ID))
                .where(MEMBERS.ID.eq(memberId))
                .fetch();
        for (Record3<String, Short, Short> bDur : borrowableDurations) {
            Short value = bDur.getValue(ITEM_CATEGORIES.BORROWABLE_HOURS);
            borrowableAmountMapper.put(bDur.getValue(ITEM_CATEGORIES.PREFIX), null != value ? value : bDur.getValue(BORROW_DETAIL_RULES.SPECIAL_BORROW_HOURS));
        }
        List<String> borrowablePrefixes = borrowableDurations.getValues(ITEM_CATEGORIES.PREFIX);

        tbmItem = new DefaultTableModel();

        tblItem.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        for (String columnName : new String[]{
                "ID",
                "Full Code",
                "Title",
                "Category",
                "Condition",
                "Due Date"
        }) {
            tbmItem.addColumn(columnName);
        }

        refreshTable();

        tblItem.setModel(tbmItem);
        TableColumnModel tableColumnModel = tblItem.getColumnModel();
        tableColumnModel.removeColumn(tableColumnModel.getColumn(0));


        txtItemCode.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                action();
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                action();
            }

            @Override
            public void changedUpdate(DocumentEvent e) {

            }

            private void action() {
                btnAdd.setEnabled(!"".equals(txtItemCode.getText()));
            }
        });

        btnAdd.addActionListener(e -> {
            String itemCode = txtItemCode.getText();
            if (itemCode.matches(Regex.ITEM_CODE)) {
                String[] codes = itemCode.split("\\.");
                String prefixCode = codes[0].split("\\-")[0];
                if (borrowablePrefixes.contains(prefixCode)) {
                    Record item = database
                            .context()
                            .select()
                            .from(ITEMS)
                            .innerJoin(ITEM_DETAILS).on(ITEMS.ITEM_CODE.eq(ITEM_DETAILS.ITEM_CODE))
                            .innerJoin(ITEM_CATEGORIES).on(ITEM_CATEGORIES.ID.eq(ITEMS.CATEGORY_ID))
                            .where(ITEMS.ITEM_CODE.eq(codes[0]).and(ITEM_DETAILS.ITEM_SUB_CODE.eq(codes[1])))
                            .fetchOne();
                    if (null != item) {
                        if (1 == item.getValue(ITEM_DETAILS.IS_ON_SHELF)) {
                            if (items.stream().noneMatch(record -> record.getValue(ITEMS.ITEM_CODE).equals(item.getValue(ITEMS.ITEM_CODE))) && items.add(item)) {
                                refreshTable();
                                btnAdd.setEnabled(--borrowableAmount > 0);
                                btnAdd.setText("Add (" + borrowableAmount + ")");
                            } else {
                                DialogUtils.showWarningMsg(LoanTransactionForm.this, "This item is already added.");
                            }
                        } else {
                            DialogUtils.showWarningMsg(LoanTransactionForm.this, "How can member get this item, it's on loan!");
                        }
                    } else {
                        DialogUtils.showWarningMsg(LoanTransactionForm.this, "Item with code '" + txtItemCode.getText() + "' does not exists");
                    }
                } else {
                    if (Arrays.asList("BK", "JN", "CP", "DV", "RT").contains(prefixCode)) DialogUtils.showWarningMsg(LoanTransactionForm.this, "This item is restricted to be loan to this member");
                    else DialogUtils.showWarningMsg(LoanTransactionForm.this, "Invalid code");
                }
            } else {
                DialogUtils.showWarningMsg(LoanTransactionForm.this, "Invalid item code format (XX-#####.###)");
            }
        });

        btnLoan.addActionListener(e -> {
            if (AuthenticateUtils.isAuthenticated(LoanTransactionForm.this, Main.activeUsername, "your")) {
                for (Record item : items) {
                    Short borrowableHours = borrowableAmountMapper.get(item.getValue(ITEM_CATEGORIES.PREFIX));
                    Routines.addLoan(
                            database.context().configuration(),
                            memberId,
                            item.getValue(ITEM_DETAILS.ID),
                            Integer.valueOf(Main.activeUserId),
                            null != borrowableHours ? Timestamp.valueOf(LocalDateTime.now().plusHours(borrowableHours)) :
                                    Timestamp.valueOf(LocalDate.now().plusDays(1).atTime(LocalTime.of(8, 0)))
                    );
                }
                DialogUtils.showInfoMsg(LoanTransactionForm.this, "Successfully loan item(s)!");
                goBack();
            }
        });

        addBackListener(btnBack);
    }

    private void refreshTable() {
        tbmItem.setRowCount(0);
        for (Record item : items) {
            Short borrowableHours = borrowableAmountMapper.get(item.getValue(ITEM_CATEGORIES.PREFIX));
            tbmItem.addRow(new String[] {
                    item.getValue(ITEM_DETAILS.ID).toString(),
                    item.getValue(ITEMS.ITEM_CODE).concat(".").concat(item.getValue(ITEM_DETAILS.ITEM_SUB_CODE)),
                    item.get(ITEMS.TITLE),
                    item.get(ITEM_CATEGORIES.NAME),
                    item.getValue(ITEM_DETAILS.CONDITION),
                    null != borrowableHours ? LocalDateTimeFormatter.format(LocalDateTime.now().plusHours(borrowableHours)) :
                            LocalDateTimeFormatter.format(LocalDate.now().plusDays(1).atTime(LocalTime.of(8, 0)))
            });
        }
        btnLoan.setEnabled(items.size() > 0);

    }

    private void createUIComponents() {
        tblItem = new JTable() {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        tblItem.getTableHeader().setReorderingAllowed(false);
    }
}
