package Admin.LoanReturnTransaction;

import Admin.Application.Main;
import Database.generated.Routines;
import Utils.*;
import org.jooq.Record;
import org.jooq.Result;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;
import java.awt.*;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Timer;
import java.util.TimerTask;

import static Admin.Application.Main.database;
import static Database.generated.tables.Employees.EMPLOYEES;
import static Database.generated.tables.Histories.HISTORIES;
import static Database.generated.tables.ItemCategories.ITEM_CATEGORIES;
import static Database.generated.tables.ItemDetails.ITEM_DETAILS;
import static Database.generated.tables.Items.ITEMS;
import static Database.generated.tables.Members.MEMBERS;

public class ReturnTransactionForm extends SmartJFrame {
    private Integer memberId;
    private JPanel view;
    private JTable tblItem;
    private JButton btnAccept;
    private JButton btnBack;
    private DefaultTableModel tbmItem;
    private Result<Record> onLoan;

    public ReturnTransactionForm(Component parent, String memberId) throws HeadlessException {
        super(parent);
        this.memberId = Integer.valueOf(memberId);
        initializeForm();
        setTitle("Return Transaction");
        setContentPane(view);
        setSize(800,500);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setLocationRelativeTo(parent);
        setResizable(false);
        setVisible(true);
    }

    @Override
    public void initializeForm() {
        tbmItem = new DefaultTableModel();

        tblItem.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        for (String columnName : new String[]{
                "History ID",
                "Item Detail ID",
                "Full Code",
                "Title",
                "Category",
                "Due Date",
                "Charge Amt.",
                "Loan by"
        }) {
            tbmItem.addColumn(columnName);
        }

        tblItem.getSelectionModel().addListSelectionListener(e -> {
            btnAccept.setEnabled(!tblItem.getSelectionModel().isSelectionEmpty());
        });

        refreshTableAndModel();

        tblItem.setModel(tbmItem);
        TableColumnModel tableColumnModel = tblItem.getColumnModel();
        tableColumnModel.removeColumn(tableColumnModel.getColumn(0));
        tableColumnModel.removeColumn(tableColumnModel.getColumn(0));

        btnAccept.addActionListener(e -> {
            if (AuthenticateUtils.isAuthenticated(ReturnTransactionForm.this, Main.activeUsername, "your")) {
                Routines.acceptReturn(
                        database.context().configuration(),
                        Integer.valueOf(getColumnOfSelectedRow(tblItem, 0).toString()),
                        Integer.valueOf(getColumnOfSelectedRow(tblItem, 1).toString()),
                        Integer.valueOf(Main.activeUserId),
                        getChargeAmount(onLoan.get(findItemIndexInModelByCode(onLoan, Integer.valueOf(getColumnOfSelectedRow(tblItem, 1).toString()), ITEM_DETAILS.ID)))
                );
                DialogUtils.showInfoMsg(ReturnTransactionForm.this, "Successfully accept return!");
                refreshTableAndModel();
            }
        });

        addBackListener(btnBack);

        long period = ChronoUnit.MINUTES.getDuration().toMillis();
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                int toBeSelected = -1;
                if (!tblItem.getSelectionModel().isSelectionEmpty()) {
                    toBeSelected = tblItem.convertRowIndexToModel(tblItem.getSelectedRow());
                }
                refreshTableAndModel();
                if (-1 != toBeSelected) {
                    int viewIndex = tblItem.convertRowIndexToView(toBeSelected);
                    tblItem.setRowSelectionInterval(viewIndex, viewIndex);
                }

            }
        }, period, period);
    }


    private void refreshTableAndModel() {
        onLoan = database.context()
                .select()
                .from(MEMBERS)
                .innerJoin(HISTORIES).on(MEMBERS.ID.eq(HISTORIES.MEMBER_ID))
                .innerJoin(ITEM_DETAILS).on(ITEM_DETAILS.ID.eq(HISTORIES.ITEM_DETAIL_ID))
                .innerJoin(ITEMS).on(ITEMS.ITEM_CODE.eq(ITEM_DETAILS.ITEM_CODE))
                .innerJoin(ITEM_CATEGORIES).on(ITEM_CATEGORIES.ID.eq(ITEMS.CATEGORY_ID))
                .innerJoin(EMPLOYEES).on(EMPLOYEES.ID.eq(HISTORIES.LOAN_EMPLOYEE_ID))
                .where(MEMBERS.ID.eq(memberId).and(HISTORIES.RETURN_DATE.isNull()))
                .fetch();
        tbmItem.setRowCount(0);
        for (Record item : onLoan) {
            tbmItem.addRow(new String[] {
                    item.getValue(HISTORIES.ID).toString(),
                    item.getValue(ITEM_DETAILS.ID).toString(),
                    item.getValue(ITEMS.ITEM_CODE).concat(".").concat(item.getValue(ITEM_DETAILS.ITEM_SUB_CODE)),
                    item.get(ITEMS.TITLE),
                    item.get(ITEM_CATEGORIES.NAME),
                    LocalDateTimeFormatter.format(item.get(HISTORIES.DUE_DATE).toLocalDateTime()),
                    "$" + getChargeAmount(item),
                    item.get(EMPLOYEES.NAME)
            });
        }

    }

    private Short getChargeAmount(Record item) {
        LocalDateTime now = LocalDateTime.now();
        LocalDateTime dueDate = item.get(HISTORIES.DUE_DATE).toLocalDateTime();
        return (now.isBefore(dueDate) ? 0 :
                Long.valueOf(item.get(ITEM_CATEGORIES.OVERDUE_CHARGE) * DurationUtils.durationBetween(dueDate, now, item.get(ITEM_CATEGORIES.OVERDUE_CHARGE_UNIT).equals("Day") ? ChronoUnit.DAYS : ChronoUnit.HOURS)).shortValue());
    }

    private void createUIComponents() {
        tblItem = new JTable() {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        tblItem.getTableHeader().setReorderingAllowed(false);
    }
}
