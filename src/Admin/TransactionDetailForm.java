package Admin;

import Admin.BuyTransaction.BuyTransactionForm;
import Admin.LoanReturnTransaction.LoanReturnTransactionForm;
import Utils.FormSwitching;
import Utils.SmartJFrame;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class TransactionDetailForm extends SmartJFrame {
    private JPanel view;
    private JLabel lblBuyTransaction;
    private JLabel lblLoanTransaction;
    private JLabel btnBack;

    public TransactionDetailForm(Component parent) throws HeadlessException {
        super(parent);
        initializeForm();
        setTitle("Transaction Dashboard Form");
        setContentPane(view);
        setSize(740,520);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setLocationRelativeTo(parent);
        setResizable(false);
        setVisible(true);
    }

    @Override
    public void initializeForm() {

        lblBuyTransaction.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                FormSwitching.switchForm(TransactionDetailForm.this, BuyTransactionForm.class);
            }
        });

        lblLoanTransaction.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                FormSwitching.switchForm(TransactionDetailForm.this, LoanReturnTransactionForm.class);
            }
        });

        addBackListener(btnBack);
    }
}
