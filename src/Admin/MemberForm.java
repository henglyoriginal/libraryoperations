package Admin;

import Database.generated.Routines;
import Utils.*;
import org.jooq.Record;
import org.jooq.Result;
import org.jooq.TableField;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.sql.Date;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Objects;

import static Admin.Application.Main.database;
import static Database.generated.tables.Members.MEMBERS;

public class MemberForm extends SmartCRUDJFrame {
    private JPanel view;
    private JTextField txtName;
    private JTextField txtAddress;
    private JRadioButton rbFemale;
    private JRadioButton rbMale;
    private JButton btnNewOrAdd;
    private JButton btnUpdate;
    private JButton btnDelete;
    private JButton btnBack;
    private JComboBox cboSearch;
    private JTextField txtSearch;
    private JTable tblMember;
    private JComboBox cboCategory;
    private JTextField txtRegistrationDate;
    private JTextField txtExpire;
    private ButtonGroup sexGroup;
    private Result<Record> categories;
    private LocalDate now = LocalDate.now(ZoneId.of("GMT+7"));

    public MemberForm(Component parent) throws HeadlessException {
        super(parent);
        initializeForm();
        setTitle("Member Form");
        setContentPane(view);
        setSize(550,600);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setLocationRelativeTo(parent);
        setResizable(false);
        setVisible(true);
    }

    public void initializeForm() {

        sexGroup = new ButtonGroup();
        sexGroup.add(rbFemale);
        sexGroup.add(rbMale);

        cboCategory.addItemListener(e -> {
            String categoryId = ((ComboItem<String, Byte>) cboCategory.getSelectedItem()).getValue().toString();
            txtExpire.setText(getExpirationForDisplay(categoryId));
        });

        categories = PopulateUtils.populateCategory(cboCategory);
        txtRegistrationDate.setText(LocalDateTimeFormatter.format(now));

        initializeTableComponents(
                tblMember,
                new String[] {
                        "ID",
                        "Name",
                        "Sex",
                        "Registration Date",
                        "Expire Date"
                },
                new int[] {0},
                new ValueWrapper[] {
                        new ValueWrapper("Name", MEMBERS.NAME),
                        new ValueWrapper("Address", MEMBERS.ADDRESS)
                },
                btnNewOrAdd,
                btnUpdate,
                btnDelete,
                null,
                cboSearch,
                txtSearch
        );

        addBackListener(btnBack);
    }

    private String getExpirationForDisplay(String categoryId) {
        Date date = getExpirationDate(categoryId);
        return null == date ? "Until off Duty" : LocalDateTimeFormatter.format(date.toLocalDate());
    }

    private Date getExpirationDate(String categoryId) {
        return Arrays.asList("1", "6").contains(categoryId) ? null : Date.valueOf(now.plusMonths(6));
    }

    @Override
    protected <T, R extends Record> TableField<R, T> getPrimaryField() {
        return (TableField<R, T>) MEMBERS.ID;
    }

    @Override
    protected Result<Record> refreshTableAndModel(DefaultTableModel tableModel) {
        Result<Record> model = database.context()
                .select()
                .from(MEMBERS)
                .where(searchMapper.get(Objects.requireNonNull(cboSearch.getSelectedItem()).toString()).like("%" + txtSearch.getText() + "%"))
                .orderBy(MEMBERS.REGISTRATION_DATE.desc())
                .fetch();
        tableModel.setRowCount(0);
        for (Record employee : model) {
            Date expirationDate = employee.getValue(MEMBERS.EXPIRE_DATE);
            tableModel.addRow(new String[] {
                    employee.getValue(MEMBERS.ID).toString(),
                    employee.getValue(MEMBERS.NAME),
                    employee.getValue(MEMBERS.SEX),
                    LocalDateTimeFormatter.format(employee.getValue(MEMBERS.REGISTRATION_DATE).toLocalDate()),
                    null == expirationDate ? "Until off Duty" : LocalDateTimeFormatter.format(expirationDate.toLocalDate())
            });
        }
        return model;
    }

    @Override
    protected void clearControl() {
        ControlUtils.clearControls(
                txtName,
                txtAddress
        );
        rbFemale.setSelected(true);
        cboCategory.setSelectedIndex(0);
        txtRegistrationDate.setText(LocalDateTimeFormatter.format(now));

        btnUpdate.setEnabled(false);
        btnDelete.setEnabled(false);
    }

    @Override
    protected void toggleControls(boolean enabled) {
        ControlUtils.toggleControl(
                enabled,
                txtName,
                rbFemale,
                rbMale,
                txtAddress,
                cboCategory
        );
    }

    @Override
    protected void onNewClickAfterControlToggle() {
        super.onNewClickAfterControlToggle();
        ControlUtils.toggleControl(true, cboCategory);
    }

    @Override
    protected void onTableItemSelectedAfterToggle() {
        super.onTableItemSelectedAfterToggle();
        ControlUtils.toggleControl(false, cboCategory);
    }

    @Override
    protected boolean validateControls() {
        return ControlUtils.validateControl(parent,
                new ValidationWrapper(txtName, component -> !"".equals(((JTextField)component).getText()), "Name can't be empty")
        );
    }

    @Override
    protected void setContent(Record record) {
        String sex = record.getValue(MEMBERS.SEX);
        txtName.setText(record.getValue(MEMBERS.NAME));
        if (null == sex || "F".equals(sex)) rbFemale.setSelected(true);
        else rbMale.setSelected(true);
        txtAddress.setText(record.getValue(MEMBERS.ADDRESS));
        Date expirationDate = record.getValue(MEMBERS.EXPIRE_DATE);
        txtExpire.setText(null == expirationDate ? "Until off Duty" : LocalDateTimeFormatter.format(expirationDate.toLocalDate()));
        cboCategory.setSelectedIndex(findItemIndexInModelByCode(categories, record.getValue(MEMBERS.CATEGORY_ID), getPrimaryField()));
        txtRegistrationDate.setText(LocalDateTimeFormatter.format(record.getValue(MEMBERS.REGISTRATION_DATE).toLocalDate()));
    }

    @Override
    protected Runnable getAddItemAction() {
        return () -> {
            Byte categoryId = ((ComboItem<String, Byte>) cboCategory.getSelectedItem()).getValue();
            Routines.addMember(
                    database.context().configuration(),
                    txtName.getText(),
                    rbFemale.isSelected() ? "F" : "M",
                    txtAddress.getText(),
                    Byte.valueOf("1"),
                    Date.valueOf(now),
                    getExpirationDate(categoryId.toString()),
                    categoryId
            );
            txtName.setEnabled(false);
            txtAddress.setEnabled(false);
            rbFemale.setEnabled(false);
            rbMale.setEnabled(false);
            cboCategory.setSelectedIndex(0);
        };
    }

    @Override
    protected Runnable getUpdateItemAction() {
        return () -> Routines.updateMember(
                database.context().configuration(),
                Integer.valueOf(getColumnOfSelectedRow(tblMember, 0).toString()),
                txtName.getText(),
                rbFemale.isSelected() ? "F" : "M",
                txtAddress.getText(),
                ((ComboItem<String, Byte>) cboCategory.getSelectedItem()).getValue()
        );
    }

    @Override
    protected Runnable getDeleteItemAction() {
        return () -> Routines.removeMember(
                database.context().configuration(),
                Integer.valueOf(getColumnOfSelectedRow(tblMember, 0).toString())
        );
    }

    private void createUIComponents() {
        tblMember = new JTable() {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        tblMember.getTableHeader().setReorderingAllowed(false);
    }
}
