package Admin;

import Admin.Application.Main;
import Admin.Common.EmployeeAddListener;
import Database.generated.Routines;
import Utils.*;
import org.jooq.Configuration;
import org.jooq.Record;
import org.jooq.Result;
import org.jooq.TableField;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.sql.Date;
import java.time.LocalDate;
import java.util.Objects;

import static Admin.Application.Main.database;
import static Database.generated.tables.Employees.EMPLOYEES;

public class EmployeeForm extends SmartCRUDJFrame {
    private JPanel view;
    private JTextField txtName;
    private JTextField txtUsername;
    private JTextField txtAddress;
    private JPasswordField txtPassword;
    private JPasswordField txtConfirmPassword;
    private JPanel datePickerLayout;
    private JComboBox cboEducation;
    private JRadioButton rbFemale;
    private JRadioButton rbMale;
    private JComboBox cboSearch;
    private JTextField txtSearch;
    private JTextField txtContactNumber;
    private JTable tblEmployee;
    private JButton btnNewOrAdd;
    private JButton btnUpdate;
    private JButton btnDelete;
    private JButton btnBack;
    private SmartDatePicker datePicker;
    private ButtonGroup sexGroup;
    private Result<Record> educations;
    private EmployeeAddListener employeeAddListener;

    private enum Action {UPDATE, DELETE}

    public EmployeeForm(Component parent) throws HeadlessException {
        super(parent);
        initializeForm();
        setTitle("Employee Form");
        setContentPane(view);
        setSize(500,600);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setLocationRelativeTo(parent);
        setResizable(false);
        setVisible(true);
    }

    public void initializeForm() {

        datePicker = initializeDatePicker(datePickerLayout);

        sexGroup = new ButtonGroup();
        sexGroup.add(rbFemale);
        sexGroup.add(rbMale);

        educations = PopulateUtils.populateEducation(cboEducation);

        initializeTableComponents(
                tblEmployee,
                new String[] {
                        "ID",
                        "Name",
                        "Sex",
                        "Hired Date",
                        "Contact Number"
                },
                new int[] {0},
                new ValueWrapper[] {
                        new ValueWrapper("Name", EMPLOYEES.NAME),
                        new ValueWrapper("Address", EMPLOYEES.ADDRESS),
                        new ValueWrapper("Contact", EMPLOYEES.CONTACT_NUMBER)
                },
                btnNewOrAdd,
                btnUpdate,
                btnDelete,
                null,
                cboSearch,
                txtSearch
        );

        addBackListener(btnBack);

        employeeAddListener = new EmployeeAddListener(
                parent,
                txtName,
                rbFemale,
                txtAddress,
                datePicker,
                txtContactNumber,
                txtUsername,
                txtPassword,
                txtConfirmPassword,
                cboEducation,
                null
        );
    }

    private boolean tryToAuthenticate(Action action) {
        String whose;
        String who;
        String selectedId = getColumnOfSelectedRow(tblEmployee, 0).toString();
        if (selectedId.equals(Main.activeUserId)) {
            if (Action.DELETE.ordinal() == action.ordinal()) {
                DialogUtils.showErrorMsg(EmployeeForm.this, "You can't delete yourself!");
                return false;
            }
            whose = "your";
            who = txtUsername.getText();
        } else {
            whose = "admin's";
            who = "admin";
        }
        return AuthenticateUtils.isAuthenticated(this, who, whose);
    }

    @Override
    protected <T, R extends Record> TableField<R, T> getPrimaryField() {
        return (TableField<R, T>) EMPLOYEES.ID;
    }

    @Override
    protected Result<Record> refreshTableAndModel(DefaultTableModel tableModel) {
        Result<Record> model = database.context()
                .select()
                .from(EMPLOYEES)
                .where(searchMapper.get(Objects.requireNonNull(cboSearch.getSelectedItem()).toString()).like("%" + txtSearch.getText() + "%").and(EMPLOYEES.IS_ADMIN.eq(Byte.valueOf("0"))))
                .orderBy(EMPLOYEES.HIRED_DATE.desc())
                .fetch();
        tableModel.setRowCount(0);
        for (Record employee : model) {
            tableModel.addRow(new String[] {
                    employee.getValue(EMPLOYEES.ID).toString(),
                    employee.getValue(EMPLOYEES.NAME),
                    employee.getValue(EMPLOYEES.SEX),
                    LocalDateTimeFormatter.format(employee.getValue(EMPLOYEES.HIRED_DATE).toLocalDate()),
                    employee.getValue(EMPLOYEES.CONTACT_NUMBER)
            });
        }
        return model;
    }

    @Override
    protected void clearControl() {
        ControlUtils.clearControls(
                txtName,
                txtAddress,
                datePicker.getComponentDateTextField(),
                txtContactNumber,
                txtUsername,
                txtPassword,
                txtConfirmPassword
        );
        rbFemale.setSelected(true);
        cboEducation.setSelectedIndex(0);
    }

    @Override
    protected void toggleControls(boolean enabled) {
        ControlUtils.toggleControl(
                enabled,
                txtName,
                rbFemale,
                rbMale,
                txtAddress,
                datePicker,
                txtContactNumber,
                cboEducation,
                txtUsername,
                txtPassword,
                txtConfirmPassword
        );
        datePicker.getComponentDateTextField().setEnabled(false);
    }

    @Override
    protected void onNewClickAfterControlToggle() {
        super.onNewClickAfterControlToggle();
        ControlUtils.toggleControl(true, txtUsername);
    }

    @Override
    protected void onTableItemSelectedAfterToggle() {
        super.onTableItemSelectedAfterToggle();
        ControlUtils.toggleControl(false, txtUsername);
    }

    @Override
    protected boolean validateControls() {
        return true;
    }

    @Override
    protected void setContent(Record record) {
        String sex = record.getValue(EMPLOYEES.SEX);
        txtName.setText(record.getValue(EMPLOYEES.NAME));
        if (null == sex || "F".equals(sex)) rbFemale.setSelected(true);
        else rbMale.setSelected(true);
        txtAddress.setText(record.getValue(EMPLOYEES.ADDRESS));
        datePicker.setDate(LocalDate.parse(record.getValue(EMPLOYEES.HIRED_DATE).toString()));
        txtContactNumber.setText(record.getValue(EMPLOYEES.CONTACT_NUMBER));
        cboEducation.setSelectedIndex(findItemIndexInModelByCode(educations, record.getValue(EMPLOYEES.EDUCATION_ID), getPrimaryField()));
        txtUsername.setText(record.getValue(EMPLOYEES.USERNAME));
        txtPassword.setText("");
        txtConfirmPassword.setText("");
    }

    @Override
    protected Runnable getAddItemAction() {
        return () -> employeeAddListener.actionPerformed(null);
    }

    @Override
    protected boolean getAddCustomValidation() {
        return super.getAddCustomValidation() &&
                employeeAddListener.validateControls(true, true);
    }

    @Override
    protected Runnable getUpdateItemAction() {
        return new Runnable() {
            @Override
            public void run() {

                //TODO: fix mistakenly update password
                Configuration configuration = database.context().configuration();
                Integer id = Integer.valueOf(EmployeeForm.this.getColumnOfSelectedRow(tblEmployee, 0).toString());
                String name = txtName.getText();
                String sex = rbFemale.isSelected() ? "F" : "M";
                String address = txtAddress.getText();
                Date hiredDate = new Date(datePicker.getDate().toEpochDay() * 86400000);
                String contactNumber = txtContactNumber.getText();
                String username = txtUsername.getText();
                Byte educationId = ((ComboItem<String, Byte>) cboEducation.getSelectedItem()).getValue();
                String password = new String(txtPassword.getPassword());
                if (password.length() > 0) {
                    Routines.updateEmployee(
                            configuration,
                            id,
                            name,
                            sex,
                            address,
                            hiredDate,
                            contactNumber,
                            username,
                            Hasher.hash(password),
                            educationId
                    );
                } else {
                    Routines.updateEmployeeWithoutPassword(
                            configuration,
                            id,
                            name,
                            sex,
                            address,
                            hiredDate,
                            contactNumber,
                            username,
                            educationId
                    );
                }

            }
        };
    }

    @Override
    protected boolean getUpdateCustomValidation() {
        String newPassword = new String(txtPassword.getPassword());
        String newConfirmPassword = new String(txtConfirmPassword.getPassword());
        return super.getUpdateCustomValidation() &&
                employeeAddListener.validateControls(
                        newPassword.length() > 0 ||
                                newConfirmPassword.length() > 0, false) &&
                tryToAuthenticate(Action.UPDATE);
    }

    @Override
    protected Runnable getDeleteItemAction() {
        return () -> Routines.removeEmployee(
                database.context().configuration(),
                Integer.valueOf(getColumnOfSelectedRow(tblEmployee, 0).toString())
        );
    }

    @Override
    protected boolean getDeleteCustomValidation() {
        return tryToAuthenticate(Action.DELETE);
    }

    private void createUIComponents() {
        tblEmployee = new JTable() {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        tblEmployee.getTableHeader().setReorderingAllowed(false);
    }
}
