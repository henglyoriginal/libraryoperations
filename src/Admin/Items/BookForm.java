package Admin.Items;

import Admin.Author.AuthorForm;
import Database.generated.Routines;
import Utils.*;
import org.jooq.Field;
import org.jooq.Record;
import org.jooq.Result;
import org.jooq.TableField;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.util.List;
import java.util.Objects;

import static Admin.Application.Main.database;
import static Database.generated.tables.Authors.AUTHORS;
import static Database.generated.tables.Books.BOOKS;
import static Database.generated.tables.DivisionSubjects.DIVISION_SUBJECTS;
import static Database.generated.tables.Items.ITEMS;
import static Database.generated.tables.Subjects.SUBJECTS;

public class BookForm extends SmartItemCRUDJFrame {
    private JPanel view;
    private JTextField txtItemCode;
    private JTextField txtTitle;
    private JButton btnNewOrAdd;
    private JButton btnUpdate;
    private JButton btnDelete;
    private JButton btnBack;
    private JComboBox cboMainSubject;
    private JComboBox cboDivisionSubject;
    private JLabel lblAmount;
    private JButton btnDetail;
    private JComboBox cboSearch;
    private JTextField txtSearch;
    private JLabel lblPrefix;
    private JTable tblBook;
    private JTextField txtPublicationYear;
    private JTextField txtISBN;
    private JTextField txtKeyword;
    private JComboBox cboAuthor;
    private JButton btnToAuthor;
    private Result<Record> authors;

    public BookForm(Component parent) throws HeadlessException {
        super(parent);
        initializeForm();
        setTitle("Book Form");
        setContentPane(view);
        setSize(600,620);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setLocationRelativeTo(parent);
        setResizable(false);
        setVisible(true);
    }

    @Override
    public void initializeForm() {

        populateAuthors();

        initializeTableComponents(
                tblBook,
                new String[]{
                        "Item Code",
                        "Title",
                        "Code",
                        "Pub. Year",
                        "Author"
                },
                new int[] {},
                new ValueWrapper[]{
                        new ValueWrapper("Item Code", BOOKS.ITEM_CODE),
                        new ValueWrapper("Title", ITEMS.TITLE),
                        new ValueWrapper("ISBN", BOOKS.ISBN),
                        new ValueWrapper("Keyword", BOOKS.KEYWORD)
                },
                btnNewOrAdd,
                btnUpdate,
                btnDelete,
                btnDetail,
                cboSearch,
                txtSearch,
                txtItemCode,
                txtTitle,
                cboMainSubject,
                cboDivisionSubject,
                lblAmount,
                lblPrefix
        );

        btnToAuthor.addActionListener(e -> {
            toBeSelectedIndex = -1;
            FormSwitching.switchForm(BookForm.this, AuthorForm.class);
        });

        addBackListener(btnBack);

    }

    private void populateAuthors() {
        authors = PopulateUtils.populateAuthor(cboAuthor);
    }

    @Override
    protected String getPrefix() {
        return "BK";
    }

    @Override
    protected <T, R extends Record> TableField<R, T> getPrimaryField() {
        return (TableField<R, T>) ITEMS.ITEM_CODE;
    }

    @Override
    protected Result<Record> refreshTableAndModel(DefaultTableModel tableModel) {
        Field<String> mapperValue = searchMapper.get(Objects.requireNonNull(cboSearch.getSelectedItem()).toString());
        String searchValue = txtSearch.getText();
        Result<Record> model = PopulateUtils.getBooks()
                .where(ITEMS.CATEGORY_ID.eq(Byte.valueOf("1")).and(mapperValue == BOOKS.ITEM_CODE && !"".equals(searchValue) ? mapperValue.eq(getPrefix() + "-" + searchValue) : mapperValue.like("%" + searchValue + "%")))
                .fetch();
        tableModel.setRowCount(0);
        for (Record item : model) {
            Short publicationYear = item.getValue(BOOKS.PUBLICATION_YEAR);
            tableModel.addRow(new String[] {
                    item.getValue(ITEMS.ITEM_CODE),
                    item.getValue(ITEMS.TITLE),
                    item.getValue(SUBJECTS.CODE).concat(".").concat(item.getValue(DIVISION_SUBJECTS.CODE)),
                    null != publicationYear ? publicationYear.toString() : "-",
                    item.getValue(AUTHORS.NAME)
            });
        }
        return model;
    }

    @Override
    protected void clearControl() {
        super.clearControl();
        ControlUtils.clearControls(
                txtISBN,
                txtPublicationYear,
                txtKeyword
        );
        if (cboAuthor.getItemCount() > 0) cboAuthor.setSelectedIndex(0);
    }

    @Override
    protected void toggleControls(boolean enabled) {
        super.toggleControls(enabled);
        ControlUtils.toggleControl(
                enabled,
                txtISBN,
                txtPublicationYear,
                txtKeyword,
                cboAuthor
        );
    }

    @Override
    protected boolean validateControls() {
        return super.validateControls() &&  ControlUtils.validateControl(BookForm.this,
                new ValidationWrapper(txtISBN, component -> {
                    String text = ((JTextField) component).getText();
                    return !"".equals(text) && text.matches(Regex.ISBN);
                }, "ISBN can't be empty and is in ISBN's format"),
                new ValidationWrapper(txtPublicationYear, component -> {
                    String text = ((JTextField) component).getText();
                    return "".equals(text) || text.matches(Regex.THREE_TO_FOUR_DIGITS);
                }, "Publication year can't be empty and must be a three-or-four-digit number"),
                new ValidationWrapper(txtKeyword, component -> !"".equals(((JTextField) component).getText()), "Keyword can't be empty"),
                new ValidationWrapper(cboAuthor, component -> {
                    JComboBox comboBox = (JComboBox) component;
                    return comboBox.getItemCount() > 0 && -1 != comboBox.getSelectedIndex();
                }, "Author can't be empty")
        );
    }

    @Override
    protected void setContent(Record record) {
        super.setContent(record);
        Short publicationYear = record.getValue(BOOKS.PUBLICATION_YEAR);
        txtISBN.setText(record.getValue(BOOKS.ISBN));
        txtPublicationYear.setText(null != publicationYear ? publicationYear.toString() : "");
        txtKeyword.setText(record.getValue(BOOKS.KEYWORD));
        cboAuthor.setSelectedIndex(findItemIndexInModelByCode(authors, record.getValue(AUTHORS.ID), AUTHORS.ID));
    }

    @Override
    protected Runnable getAddItemAction() {
        return () -> {
            List<String> bookIds = getModel()
                    .sortDesc(BOOKS.ITEM_CODE)
                    .getValues(BOOKS.ITEM_CODE);
            String itemCode = ItemIdGenerator.generate(bookIds, "BK");
            String publicationYearText = txtPublicationYear.getText();
            Routines.addBook(
                    database.context().configuration(),
                    txtTitle.getText(),
                    itemCode,
                    ((ComboItem<String, Integer>) cboDivisionSubject.getSelectedItem()).getValue(),
                    txtISBN.getText(),
                    !"".equals(publicationYearText) ? Short.valueOf(publicationYearText) : null,
                    txtKeyword.getText(),
                    ((ComboItem<String, Integer>) cboAuthor.getSelectedItem()).getValue()
            );
        };
    }

    @Override
    protected Runnable getUpdateItemAction() {
        return () ->
        {
            String publicationYearText = txtPublicationYear.getText();
            Routines.updateBook(
                    database.context().configuration(),
                    txtItemCode.getText(),
                    txtTitle.getText(),
                    ((ComboItem<String, Integer>) cboDivisionSubject.getSelectedItem()).getValue(),
                    txtISBN.getText(),
                    !"".equals(publicationYearText) ? Short.valueOf(publicationYearText) : null,
                    txtKeyword.getText(),
                    ((ComboItem<String, Integer>) cboAuthor.getSelectedItem()).getValue()
            );
        };
    }

    @Override
    protected Runnable getDeleteItemAction() {
        return () -> Routines.deleteItem(database.context().configuration(), txtItemCode.getText());
    }

    @Override
    public void setVisible(boolean b) {
        super.setVisible(b);
        if (b) populateAuthors();
    }

    private void createUIComponents() {
        tblBook = new JTable() {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        tblBook.getTableHeader().setReorderingAllowed(false);
    }

}
