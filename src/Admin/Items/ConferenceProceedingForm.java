package Admin.Items;

import Database.generated.Routines;
import Utils.*;
import org.jooq.Field;
import org.jooq.Record;
import org.jooq.Result;
import org.jooq.TableField;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.sql.Date;
import java.time.LocalDate;
import java.util.List;
import java.util.Objects;

import static Admin.Application.Main.database;
import static Database.generated.tables.ConferenceProceedings.CONFERENCE_PROCEEDINGS;
import static Database.generated.tables.DivisionSubjects.DIVISION_SUBJECTS;
import static Database.generated.tables.Items.ITEMS;
import static Database.generated.tables.Subjects.SUBJECTS;

public class ConferenceProceedingForm extends SmartItemCRUDJFrame {
    private JPanel view;
    private JTextField txtItemCode;
    private JTextField txtTitle;
    private JButton btnNewOrAdd;
    private JButton btnUpdate;
    private JButton btnDelete;
    private JButton btnBack;
    private JComboBox cboSearch;
    private JTextField txtSearch;
    private JPanel datePickerLayout;
    private JLabel lblPrefix;
    private JComboBox cboMainSubject;
    private JComboBox cboDivisionSubject;
    private JTable tblConferenceProceedings;
    private JButton btnDetail;
    private JLabel lblAmount;
    private SmartDatePicker datePicker;

    public ConferenceProceedingForm(Component parent) throws HeadlessException {
        super(parent);
        initializeForm();
        setTitle("Conference Proceeding Form");
        setContentPane(view);
        setSize(600,620);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setLocationRelativeTo(parent);
        setResizable(false);
        setVisible(true);
    }

    @Override
    public void initializeForm() {

        datePicker = initializeDatePicker(datePickerLayout);

        initializeTableComponents(
                tblConferenceProceedings,
                new String[] { "Item Code",
                        "Title",
                        "Code",
                        "Con. Date"
                },
                new int[] {},
                new ValueWrapper[]{
                        new ValueWrapper("Item Code", CONFERENCE_PROCEEDINGS.ITEM_CODE),
                        new ValueWrapper("Title", ITEMS.TITLE)
                },
                btnNewOrAdd,
                btnUpdate,
                btnDelete,
                btnDetail,
                cboSearch,
                txtSearch,
                txtItemCode,
                txtTitle,
                cboMainSubject,
                cboDivisionSubject,
                lblAmount,
                lblPrefix
        );
        addBackListener(btnBack);

    }

    @Override
    protected String getPrefix() {
        return "CP";
    }

    @Override
    protected <T, R extends Record> TableField<R, T> getPrimaryField() {
        return (TableField<R, T>) ITEMS.ITEM_CODE;
    }

    @Override
    protected Result<Record> refreshTableAndModel(DefaultTableModel tableModel) {
        Field<String> mapperValue = searchMapper.get(Objects.requireNonNull(cboSearch.getSelectedItem()).toString());
        String searchValue = txtSearch.getText();
        Result<Record> model = database.context()
                .select()
                .from(CONFERENCE_PROCEEDINGS)
                .innerJoin(ITEMS)
                .on(CONFERENCE_PROCEEDINGS.ITEM_CODE.eq(ITEMS.ITEM_CODE))
                .innerJoin(DIVISION_SUBJECTS)
                .on(DIVISION_SUBJECTS.ID.eq(ITEMS.SUBJECT_ID))
                .innerJoin(SUBJECTS)
                .on(SUBJECTS.ID.eq(DIVISION_SUBJECTS.MAIN_SUBJECT_ID))
                .where(ITEMS.CATEGORY_ID.eq(Byte.valueOf("3")).and(mapperValue == CONFERENCE_PROCEEDINGS.ITEM_CODE && !"".equals(searchValue) ? mapperValue.eq(getPrefix() + "-" + searchValue) : mapperValue.like("%" + searchValue + "%")))
                .fetch();
        tableModel.setRowCount(0);
        for (Record item : model) {
            tableModel.addRow(new String[] {
                    item.getValue(ITEMS.ITEM_CODE),
                    item.getValue(ITEMS.TITLE),
                    item.getValue(SUBJECTS.CODE).concat(".").concat(item.getValue(DIVISION_SUBJECTS.CODE)),
                    item.getValue(CONFERENCE_PROCEEDINGS.CONFERENCE_DATE).toString()
            });
        }
        return model;
    }

    @Override
    protected void clearControl() {
        super.clearControl();
        ControlUtils.clearControls(datePicker.getComponentDateTextField());
    }

    @Override
    protected void toggleControls(boolean enabled) {
        super.toggleControls(enabled);
        ControlUtils.toggleControl(enabled, datePicker);
        datePicker.getComponentDateTextField().setEnabled(false);
    }

    @Override
    protected boolean validateControls() {
        return super.validateControls() && ControlUtils.validateControl(ConferenceProceedingForm.this,
                new ValidationWrapper(datePicker.getComponentDateTextField(), component -> !"".equals(((JTextField) component).getText()), "Conference date can't be empty")
        );
    }

    @Override
    protected void setContent(Record record) {
        super.setContent(record);
        datePicker.setDate(LocalDate.parse(record.getValue(CONFERENCE_PROCEEDINGS.CONFERENCE_DATE).toString()));
    }

    @Override
    protected Runnable getAddItemAction() {
        return () -> {
            List<String> conferenceId = getModel()
                    .sortDesc(CONFERENCE_PROCEEDINGS.ITEM_CODE)
                    .getValues(CONFERENCE_PROCEEDINGS.ITEM_CODE);
            String itemCode = ItemIdGenerator.generate(conferenceId, "CP");
            Routines.addConferenceProceeding(
                    database.context().configuration(),
                    txtTitle.getText(),
                    itemCode,
                    ((ComboItem<String, Integer>) cboDivisionSubject.getSelectedItem()).getValue(),
                    new Date(datePicker.getDate().toEpochDay() * 86400000)
            );
        };
    }

    @Override
    protected Runnable getUpdateItemAction() {
        return () -> Routines.updateConferenceProceeding(
                database.context().configuration(),
                txtItemCode.getText(),
                txtTitle.getText(),
                ((ComboItem<String, Integer>) cboDivisionSubject.getSelectedItem()).getValue(),
                new Date(datePicker.getDate().toEpochDay() * 86400000)
        );
    }

    @Override
    protected Runnable getDeleteItemAction() {
        return () -> Routines.deleteItem(database.context().configuration(), getColumnOfSelectedRow(tblConferenceProceedings, 0).toString());
    }

    private void createUIComponents() {
        tblConferenceProceedings = new JTable() {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        tblConferenceProceedings.getTableHeader().setReorderingAllowed(false);
    }
}
