package Admin.Items;

import Database.generated.Routines;
import Utils.ControlUtils;
import Utils.SmartCRUDJFrame;
import org.jooq.Record;
import org.jooq.Result;
import org.jooq.TableField;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.util.List;

import static Admin.Application.Main.database;
import static Database.generated.tables.ItemDetails.*;
import static Database.generated.tables.Items.*;

public class ItemDetailForm extends SmartCRUDJFrame {
    private JTextField txtItemNumber;
    private JTextField txtTitle;
    private JButton btnNewOrAdd;
    private JButton btnUpdate;
    private JButton btnDelete;
    private JButton btnBack;
    private JComboBox cboCondition;
    private JTable tblDetail;
    private JPanel view;

    private String itemCode;
    private String title;

    public ItemDetailForm(Component parent, String itemNumber) throws HeadlessException {
        super(parent);
        this.itemCode = itemNumber;
        initializeForm();
        setTitle("ItemDetail Form");
        setContentPane(view);
        setSize(500,500);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setLocationRelativeTo(parent);
        setResizable(false);
        setVisible(true);
    }

    public void initializeForm() {

        Record entity = database.context()
                .select()
                .from(ITEMS)
                .where(ITEMS.ITEM_CODE.eq(itemCode))
                .fetchOne();
        if (null != entity) {
            title = entity.getValue(ITEMS.TITLE);
        }

        txtItemNumber.setText(itemCode);
        txtTitle.setText(title);

        initializeTableComponents(
                tblDetail,
                new String[]{"Id", "Sub Code", "Condition", "On Shelf"},
                new int[] {0},
                btnNewOrAdd,
                btnUpdate,
                btnDelete
        );

        addBackListener(btnBack);

    }

    @Override
    protected <T, R extends Record> TableField<R, T> getPrimaryField() {
        return (TableField<R, T>) ITEM_DETAILS.ID;
    }

    @Override
    protected Result<Record> refreshTableAndModel(DefaultTableModel tableModel) {
        Result<Record> model = database
                .context()
                .select()
                .from(ITEMS)
                .innerJoin(ITEM_DETAILS).on(ITEMS.ITEM_CODE.eq(ITEM_DETAILS.ITEM_CODE))
                .where(ITEMS.ITEM_CODE.eq(itemCode))
                .orderBy(ITEM_DETAILS.ITEM_SUB_CODE)
                .fetch();
        tableModel.setRowCount(0);
        for (Record item : model) {
            tableModel.addRow(new String[] {
                    item.getValue(ITEM_DETAILS.ID).toString(),
                    item.getValue(ITEM_DETAILS.ITEM_SUB_CODE),
                    item.getValue(ITEM_DETAILS.CONDITION),
                    item.getValue(ITEM_DETAILS.IS_ON_SHELF) == 1 ? "Yes" : "No"
            });
        }
        return model;
    }

    @Override
    protected void clearControl() {
        cboCondition.setSelectedIndex(0);
    }

    @Override
    protected void toggleControls(boolean enabled) {
        ControlUtils.toggleControl(enabled, cboCondition);
    }

    @Override
    protected boolean validateControls() {
        return true;
    }

    @Override
    protected void setContent(Record record) {
        cboCondition.setSelectedItem(record.getValue(ITEM_DETAILS.CONDITION));
    }

    @Override
    protected Runnable getAddItemAction() {
        return () -> {
            List<String> itemDetailIds = getModel()
                    .sortDesc(ITEM_DETAILS.ITEM_SUB_CODE)
                    .getValues(ITEM_DETAILS.ITEM_SUB_CODE);
            int size = itemDetailIds.size();
            String itemSubCode = 0 == size ? "001" : "";
            if (0 != size) {
                String id = String.valueOf(Integer.valueOf(itemDetailIds.get(0)) + 1);
                while (id.length() < 3) {
                    id = "0" + id;
                }
                itemSubCode += id;
            }

            Routines.addItemDetail(
                    database.context().configuration(),
                    itemCode,
                    itemSubCode,
                    cboCondition.getSelectedItem().toString()
            );
        };
    }

    @Override
    protected Runnable getUpdateItemAction() {
        return () -> {

            Routines.updateItemDetail(
                    database.context().configuration(),
                    itemCode,
                    getColumnOfSelectedRow(tblDetail,1).toString(),
                    cboCondition.getSelectedItem().toString()
            );
        };
    }

    @Override
    protected Runnable getDeleteItemAction() {
        return () -> Routines.deleteItemDetail(
                database.context().configuration(),
                itemCode,
                getColumnOfSelectedRow(tblDetail,1).toString()
        );
    }

    private void createUIComponents() {
        tblDetail = new JTable() {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        tblDetail.getTableHeader().setReorderingAllowed(false);
    }

}
