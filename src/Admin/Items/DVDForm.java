package Admin.Items;

import Database.generated.Routines;
import Utils.*;
import org.jooq.Field;
import org.jooq.Record;
import org.jooq.Result;
import org.jooq.TableField;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.util.List;
import java.util.Objects;

import static Admin.Application.Main.database;
import static Database.generated.tables.DivisionSubjects.DIVISION_SUBJECTS;
import static Database.generated.tables.Dvds.DVDS;
import static Database.generated.tables.Items.ITEMS;
import static Database.generated.tables.Subjects.SUBJECTS;

public class DVDForm extends SmartItemCRUDJFrame {
    private JTextField txtItemNumber;
    private JTextField txtTitle;
    private JButton btnNewOrAdd;
    private JButton btnUpdate;
    private JButton btnDelete;
    private JButton btnBack;
    private JComboBox cboMainSubject;
    private JComboBox cboDivisionSubject;
    private JLabel lblAmount;
    private JButton btnDetail;
    private JComboBox cboSearch;
    private JTextField txtSearch;
    private JLabel lblPrefix;
    private JTable tblDVDs;
    private JTextField txtProducer;
    private JTextArea txtComment;
    private JPanel view;

    public DVDForm(Component parent) throws HeadlessException {
        super(parent);
        initializeForm();
        setTitle("DVD Form");
        setContentPane(view);
        setSize(550,600);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setLocationRelativeTo(parent);
        setResizable(false);
        setVisible(true);
    }

    public void initializeForm(){

        initializeTableComponents(
                tblDVDs,
                new String[] { "Item Code",
                        "Title",
                        "Code",
                        "Producer",
                        "Comment"
                },
                new int[] {},
                new ValueWrapper[]{
                        new ValueWrapper("Item Code", DVDS.ITEM_CODE),
                        new ValueWrapper("Title", ITEMS.TITLE)
                },
                btnNewOrAdd,
                btnUpdate,
                btnDelete,
                btnDetail,
                cboSearch,
                txtSearch,
                txtItemNumber,
                txtTitle,
                cboMainSubject,
                cboDivisionSubject,
                lblAmount,
                lblPrefix
        );
        addBackListener(btnBack);
    }

    @Override
    protected String getPrefix() {
        return "DV";
    }

    @Override
    protected <T, R extends Record> TableField<R, T> getPrimaryField() {
        return (TableField<R, T>) ITEMS.ITEM_CODE;
    }

    @Override
    protected Result<Record> refreshTableAndModel(DefaultTableModel tableModel) {
        Field<String> mapperValue = searchMapper.get(Objects.requireNonNull(cboSearch.getSelectedItem()).toString());
        String searchValue = txtSearch.getText();
        Result<Record> model = database.context()
                .select()
                .from(DVDS)
                .innerJoin(ITEMS)
                .on(DVDS.ITEM_CODE.eq(ITEMS.ITEM_CODE))
                .innerJoin(DIVISION_SUBJECTS)
                .on(DIVISION_SUBJECTS.ID.eq(ITEMS.SUBJECT_ID))
                .innerJoin(SUBJECTS)
                .on(SUBJECTS.ID.eq(DIVISION_SUBJECTS.MAIN_SUBJECT_ID))
                .where(ITEMS.CATEGORY_ID.eq(Byte.valueOf("4")).and(mapperValue == DVDS.ITEM_CODE && !"".equals(searchValue) ? mapperValue.eq(getPrefix() + "-" + searchValue) : mapperValue.like("%" + searchValue + "%")))
                .fetch();
        tableModel.setRowCount(0);
        for (Record item : model) {
            tableModel.addRow(new String[] {
                    item.getValue(ITEMS.ITEM_CODE),
                    item.getValue(ITEMS.TITLE),
                    item.getValue(SUBJECTS.CODE).concat(".").concat(item.getValue(DIVISION_SUBJECTS.CODE)),
                    item.getValue(DVDS.PRODUCER),
                    item.getValue(DVDS.COMMENTS)
            });
        }
        return model;
    }

    @Override
    protected void clearControl() {
        super.clearControl();
        ControlUtils.clearControls(txtProducer);
        txtComment.setText(null);
    }

    @Override
    protected void toggleControls(boolean enabled) {
        super.toggleControls(enabled);
        ControlUtils.toggleControl(enabled, txtProducer, txtComment);
    }

    @Override
    protected boolean validateControls() {
        return super.validateControls() && ControlUtils.validateControl(DVDForm.this);
    }

    @Override
    protected void setContent(Record record) {
        super.setContent(record);
        txtComment.setText(record.getValue(DVDS.COMMENTS));
        txtProducer.setText(record.getValue(DVDS.PRODUCER));
    }

    @Override
    protected Runnable getAddItemAction() {
        return () -> {
            List<String> DvdIds = getModel()
                    .sortDesc(DVDS.ITEM_CODE)
                    .getValues(DVDS.ITEM_CODE);
            String itemCode = ItemIdGenerator.generate(DvdIds, "DV");
            Routines.addDvd(
                    database.context().configuration(),
                    txtTitle.getText(),
                    itemCode,
                    ((ComboItem<String, Integer>) cboDivisionSubject.getSelectedItem()).getValue(),
                    txtComment.getText(),
                    txtProducer.getText()
            );
        };
    }

    @Override
    protected Runnable getUpdateItemAction() {
        return () -> Routines.updateDvd(
                database.context().configuration(),
                txtItemNumber.getText(),
                txtTitle.getText(),
                ((ComboItem<String, Integer>) cboDivisionSubject.getSelectedItem()).getValue(),
                txtComment.getText(),
                txtProducer.getText()
        );
    }

    @Override
    protected Runnable getDeleteItemAction() {
        return () -> Routines.deleteItem(database.context().configuration(), getColumnOfSelectedRow(tblDVDs, 0).toString());
    }

    private void createUIComponents() {
        tblDVDs = new JTable() {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        tblDVDs.getTableHeader().setReorderingAllowed(false);
    }
}
