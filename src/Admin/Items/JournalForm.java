package Admin.Items;

import Database.generated.Routines;
import Utils.*;
import org.jooq.Field;
import org.jooq.Record;
import org.jooq.Result;
import org.jooq.TableField;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.util.List;
import java.util.Objects;

import static Admin.Application.Main.database;
import static Database.generated.tables.DivisionSubjects.DIVISION_SUBJECTS;
import static Database.generated.tables.Items.ITEMS;
import static Database.generated.tables.Journals.JOURNALS;
import static Database.generated.tables.Subjects.SUBJECTS;

public class JournalForm extends SmartItemCRUDJFrame {
    private JPanel view;
    private JTextField txtItemCode;
    private JTextField txtTitle;
    private JButton btnNewOrAdd;
    private JButton btnUpdate;
    private JButton btnDelete;
    private JButton btnBack;
    private JComboBox cboMainSubject;
    private JComboBox cboDivisionSubject;
    private JLabel lblAmount;
    private JButton btnDetail;
    private JComboBox cboSearch;
    private JTextField txtSearch;
    private JLabel lblPrefix;
    private JTable tblJournal;
    private JTextField txtYearlyPublic;
    private JTextField txtAffRS;

    public void initializeForm(){
        initializeTableComponents(
                tblJournal,
                new String[] { "Item Code",
                        "Title",
                        "Code",
                        "Yearly Freq.",
                        "Aff. RS"
                },
                new int[] {},
                new ValueWrapper[]{
                        new ValueWrapper("Item Code", JOURNALS.ITEM_CODE),
                        new ValueWrapper("Title", ITEMS.TITLE)
                },
                btnNewOrAdd,
                btnUpdate,
                btnDelete,
                btnDetail,
                cboSearch,
                txtSearch,
                txtItemCode,
                txtTitle,
                cboMainSubject,
                cboDivisionSubject,
                lblAmount,
                lblPrefix
        );
        addBackListener(btnBack);
    }

    @Override
    protected String getPrefix() {
        return "JN";
    }

    @Override
    protected <T, R extends Record> TableField<R, T> getPrimaryField() {
        return (TableField<R, T>) ITEMS.ITEM_CODE;
    }

    @Override
    protected Result<Record> refreshTableAndModel(DefaultTableModel tableModel) {
        Field<String> mapperValue = searchMapper.get(Objects.requireNonNull(cboSearch.getSelectedItem()).toString());
        String searchValue = txtSearch.getText();
        Result<Record> model = database.context()
                .select()
                .from(JOURNALS)
                .innerJoin(ITEMS)
                .on(JOURNALS.ITEM_CODE.eq(ITEMS.ITEM_CODE))
                .innerJoin(DIVISION_SUBJECTS)
                .on(DIVISION_SUBJECTS.ID.eq(ITEMS.SUBJECT_ID))
                .innerJoin(SUBJECTS)
                .on(SUBJECTS.ID.eq(DIVISION_SUBJECTS.MAIN_SUBJECT_ID))
                .where(ITEMS.CATEGORY_ID.eq(Byte.valueOf("2")).and(mapperValue == JOURNALS.ITEM_CODE && !"".equals(searchValue) ? mapperValue.eq(getPrefix() + "-" + searchValue) : mapperValue.like("%" + searchValue + "%")))
                .fetch();
        tableModel.setRowCount(0);
        for (Record item : model) {
            tableModel.addRow(new String[] {
                    item.getValue(ITEMS.ITEM_CODE),
                    item.getValue(ITEMS.TITLE),
                    item.getValue(SUBJECTS.CODE).concat(".").concat(item.getValue(DIVISION_SUBJECTS.CODE)),
                    item.getValue(JOURNALS.YEARLY_PUBLIC_FREQUENCY).toString(),
                    item.getValue(JOURNALS.AFFILIATED_RESEARCH_SOCIETY)
            });
        }
        return model;
    }

    @Override
    protected void clearControl() {
        super.clearControl();
        ControlUtils.clearControls(txtYearlyPublic, txtAffRS);
    }

    @Override
    protected void toggleControls(boolean enabled) {
        super.toggleControls(enabled);
        ControlUtils.toggleControl(enabled, txtYearlyPublic, txtAffRS);
    }

    @Override
    protected boolean validateControls() {
        return super.validateControls() && ControlUtils.validateControl(JournalForm.this,
                new ValidationWrapper(txtYearlyPublic, component -> ((JTextField)component).getText().matches(Regex.NUMBER) && !"".equals(((JTextField)component).getText()), "Yearly public freq. can't be empty and must be a number"),
                new ValidationWrapper(txtAffRS, component -> !"".equals(((JTextField)component).getText()), "Affiliated research society can't be empty")
        );
    }

    @Override
    protected void setContent(Record record) {
        super.setContent(record);
        txtYearlyPublic.setText(record.getValue(JOURNALS.YEARLY_PUBLIC_FREQUENCY).toString());
        txtAffRS.setText(record.getValue(JOURNALS.AFFILIATED_RESEARCH_SOCIETY));
    }

    @Override
    protected Runnable getAddItemAction() {
        return () -> {
            List<String> conferenceId = getModel()
                    .sortDesc(JOURNALS.ITEM_CODE)
                    .getValues(JOURNALS.ITEM_CODE);
            String itemCode = ItemIdGenerator.generate(conferenceId, "JN");
            Routines.addJournal(
                    database.context().configuration(),
                    txtTitle.getText(),
                    itemCode,
                    ((ComboItem<String, Integer>) cboDivisionSubject.getSelectedItem()).getValue(),
                    Short.valueOf(txtYearlyPublic.getText()),
                    txtAffRS.getText()
            );
        };
    }

    @Override
    protected Runnable getUpdateItemAction() {
        return () -> Routines.updateJournal(
                database.context().configuration(),
                txtItemCode.getText(),
                txtTitle.getText(),
                ((ComboItem<String, Integer>) cboDivisionSubject.getSelectedItem()).getValue(),
                Short.valueOf(txtYearlyPublic.getText()),
                txtAffRS.getText()
        );
    }

    @Override
    protected Runnable getDeleteItemAction() {
        return () -> Routines.deleteItem(database.context().configuration(), getColumnOfSelectedRow(tblJournal, 0).toString());
    }


    public JournalForm(Component parent) throws HeadlessException {
        super(parent);
        initializeForm();
        setTitle("Journal Form");
        setContentPane(view);
        setSize(590,600);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setLocationRelativeTo(parent);
        setResizable(false);
        setVisible(true);
    }

    private void createUIComponents() {
        tblJournal = new JTable() {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        tblJournal.getTableHeader().setReorderingAllowed(false);
    }
}
