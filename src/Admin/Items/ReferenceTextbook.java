package Admin.Items;

import Database.generated.Routines;
import Utils.ComboItem;
import Utils.ItemIdGenerator;
import Utils.SmartItemCRUDJFrame;
import Utils.ValueWrapper;
import org.jooq.Field;
import org.jooq.Record;
import org.jooq.Result;
import org.jooq.TableField;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.util.List;
import java.util.Objects;

import static Admin.Application.Main.database;
import static Database.generated.tables.DivisionSubjects.DIVISION_SUBJECTS;
import static Database.generated.tables.Items.ITEMS;
import static Database.generated.tables.Subjects.SUBJECTS;

public class ReferenceTextbook extends SmartItemCRUDJFrame {
    private JPanel view;
    private JTextField txtItemNumber;
    private JTextField txtTitle;
    private JButton btnNewOrAdd;
    private JButton btnUpdate;
    private JButton btnDelete;
    private JButton btnBack;
    private JComboBox cboMainSubject;
    private JComboBox cboDivisionSubject;
    private JLabel lblAmount;
    private JButton btnDetail;
    private JComboBox cboSearch;
    private JTextField txtSearch;
    private JLabel lblPrefix;
    private JTable tblReferenceTextbook;

    public ReferenceTextbook(Component parent) throws HeadlessException {
        super(parent);
        initializeForm();
        setTitle("Reference TextBook Form");
        setContentPane(view);
        setSize(550,600);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setLocationRelativeTo(parent);
        setResizable(false);
        setVisible(true);
    }

    public void initializeForm(){
        initializeTableComponents(
                tblReferenceTextbook,
                new String[] { "Item Code",
                        "Title",
                        "Code"
                },
                new int[] {},
                new ValueWrapper[]{
                        new ValueWrapper("Item Code", ITEMS.ITEM_CODE),
                        new ValueWrapper("Title", ITEMS.TITLE)
                },
                btnNewOrAdd,
                btnUpdate,
                btnDelete,
                btnDetail,
                cboSearch,
                txtSearch,
                txtItemNumber,
                txtTitle,
                cboMainSubject,
                cboDivisionSubject,
                lblAmount,
                lblPrefix
        );
        addBackListener(btnBack);
    }

    @Override
    protected String getPrefix() {
        return "RT";
    }

    @Override
    protected <T, R extends Record> TableField<R, T> getPrimaryField() {
        return (TableField<R, T>) ITEMS.ITEM_CODE;
    }

    @Override
    protected Result<Record> refreshTableAndModel(DefaultTableModel tableModel) {
        Field<String> mapperValue = searchMapper.get(Objects.requireNonNull(cboSearch.getSelectedItem()).toString());
        String searchValue = txtSearch.getText();
        Result<Record> model = database.context()
                .select()
                .from(ITEMS)
                .innerJoin(DIVISION_SUBJECTS)
                .on(DIVISION_SUBJECTS.ID.eq(ITEMS.SUBJECT_ID))
                .innerJoin(SUBJECTS)
                .on(SUBJECTS.ID.eq(DIVISION_SUBJECTS.MAIN_SUBJECT_ID))
                .where(ITEMS.CATEGORY_ID.eq(Byte.valueOf("5")).and(mapperValue == ITEMS.ITEM_CODE && !"".equals(searchValue) ? mapperValue.eq(getPrefix() + "-" + searchValue) : mapperValue.like("%" + searchValue + "%")))
                .fetch();
        tableModel.setRowCount(0);
        for (Record item : model) {
            tableModel.addRow(new String[] {
                    item.getValue(ITEMS.ITEM_CODE),
                    item.getValue(ITEMS.TITLE),
                    item.getValue(SUBJECTS.CODE).concat(".").concat(item.getValue(DIVISION_SUBJECTS.CODE))
            });
        }
        return model;
    }

    @Override
    protected Runnable getAddItemAction() {
        return () -> {
            List<String> ReferenceTextbookIds = getModel()
                    .sortDesc(ITEMS.ITEM_CODE)
                    .getValues(ITEMS.ITEM_CODE);
            String itemCode = ItemIdGenerator.generate(ReferenceTextbookIds, "RT");
            Routines.addReferenceTextbook(
                    database.context().configuration(),
                    txtTitle.getText(),
                    itemCode,
                    ((ComboItem<String, Integer>) cboDivisionSubject.getSelectedItem()).getValue()
            );
        };
    }

    @Override
    protected Runnable getUpdateItemAction() {
        return () -> Routines.updateReferenceTextbook(
                database.context().configuration(),
                txtItemNumber.getText(),
                txtTitle.getText(),
                ((ComboItem<String, Integer>) cboDivisionSubject.getSelectedItem()).getValue()
        );
    }

    @Override
    protected Runnable getDeleteItemAction() {
        return () -> Routines.deleteItem(database.context().configuration(), getColumnOfSelectedRow(tblReferenceTextbook, 0).toString());
    }

    private void createUIComponents() {
        tblReferenceTextbook = new JTable() {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        tblReferenceTextbook.getTableHeader().setReorderingAllowed(false);
    }
}
