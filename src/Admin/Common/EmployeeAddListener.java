package Admin.Common;

import Database.generated.Routines;
import Utils.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Date;

import static Admin.Application.Main.database;

public class EmployeeAddListener implements ActionListener {

    private Component parent;
    private JTextField txtName;
    private JRadioButton rbFemale;
    private JTextField txtAddress;
    private SmartDatePicker datePicker;
    private JTextField txtContactNumber;
    private JTextField txtUsername;
    private JPasswordField txtPassword;
    private JPasswordField txtConfirmPassword;
    private JComboBox cboEducation;
    private Runnable additionalSuccessAction;

    public EmployeeAddListener(
            Component parent,
            JTextField txtName,
            JRadioButton rbFemale,
            JTextField txtAddress,
            SmartDatePicker datePicker,
            JTextField txtContactNumber,
            JTextField txtUsername,
            JPasswordField txtPassword,
            JPasswordField txtConfirmPassword,
            JComboBox cboEducation,
            Runnable additionalSuccessAction
    ) {
        this.parent = parent;
        this.txtName = txtName;
        this.rbFemale = rbFemale;
        this.txtAddress = txtAddress;
        this.datePicker = datePicker;
        this.txtContactNumber = txtContactNumber;
        this.txtUsername = txtUsername;
        this.txtPassword = txtPassword;
        this.txtConfirmPassword = txtConfirmPassword;
        this.cboEducation = cboEducation;
        this.additionalSuccessAction = additionalSuccessAction;
    }

    public boolean validateControls(boolean shouldValidatePassword, boolean shouldValidateExistUsername) {
        return ControlUtils.validateControl(parent,
                new ValidationWrapper(txtName, component -> !"".equals(((JTextField)component).getText()), "Name can't be empty"),
                new ValidationWrapper(txtAddress, component -> !"".equals(((JTextField) component).getText()), "Address can't be empty"),
                new ValidationWrapper(datePicker.getComponentDateTextField(), component -> !"".equals(((JTextField) component).getText()), "Hire date can't be empty"),
                new ValidationWrapper(txtContactNumber, component -> ((JTextField) component).getText().matches(Regex.PHONE_NUMBER), "Contact must be 0########(#)"),
                new ValidationWrapper(txtUsername, component -> !"".equals(((JTextField)component).getText()), "Username can't be empty"),
                new ValidationWrapper(txtUsername, component -> !shouldValidateExistUsername || "-1".equals(database.context().select(
                        Routines.isUsernameExists(((JTextField)component).getText())
                ).fetchOne().getValue(0).toString()), "Username is already exists"),
                new ValidationWrapper(txtPassword, component -> {
                    String password = new String(((JPasswordField) component).getPassword());
                    return !shouldValidatePassword || !"".equals(password) && password.length() >= 8;
                }, "Password can't be empty and minimum of 8 characters long"),
                new ValidationWrapper(txtConfirmPassword, component -> {
                    String password = new String(((JPasswordField) component).getPassword());
                    return !shouldValidatePassword || !"".equals(password) && password.equals(new String(txtPassword.getPassword()));
                }, "Confirm password must be the same as password")

        );
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if(validateControls(true, true)) {
            Routines.signMeUp(
                    database.context().configuration(),
                    txtName.getText(),
                    rbFemale.isSelected() ? "F" : "M",
                    txtAddress.getText(),
                    new Date(datePicker.getDate().toEpochDay() * 86400000),
                    txtContactNumber.getText(),
                    txtUsername.getText(),
                    Hasher.hash(new String(txtPassword.getPassword())),
                    ((ComboItem<String, Byte>) cboEducation.getSelectedItem()).getValue()
            );
            if (null != additionalSuccessAction) additionalSuccessAction.run();
        }
    }
}
