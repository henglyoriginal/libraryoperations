package Admin;

import Database.generated.Routines;
import Utils.ControlUtils;
import Utils.SmartCRUDJFrame;
import Utils.ValidationWrapper;
import Utils.ValueWrapper;
import org.jooq.Record;
import org.jooq.Result;
import org.jooq.TableField;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.util.Objects;

import static Admin.Application.Main.database;
import static Database.generated.tables.Publishers.PUBLISHERS;

public class PublisherForm extends SmartCRUDJFrame{
    private JPanel view;
    private JTextField txtName;
    private JTextField txtAddress;
    private JTextField txtContact;
    private JButton btnNewOrAdd;
    private JButton btnBack;
    private JButton btnUpdate;
    private JButton btnDelete;
    private JTable tblPublisher;
    private JTextField txtSearch;
    private JComboBox cboSearch;

    public PublisherForm(Component parent) throws  HeadlessException{
        super(parent);
        initializeForm();
        setTitle("Publisher Form");
        setContentPane(view);
        setSize(600,620);
        setLocationRelativeTo(parent);
        setResizable(false);
        setVisible(true);
    }

    public void initializeForm() {
        initializeTableComponents(
                tblPublisher,
                new String[]{
                        "ID",
                        "Name",
                        "Address",
                        "Contact"
                },
                new int[]{0},
                new ValueWrapper[]{
                        new ValueWrapper("Name", PUBLISHERS.NAME),
                        new ValueWrapper("Address", PUBLISHERS.ADDRESS),
                        new ValueWrapper("Contact", PUBLISHERS.CONTACT_NUMBER)
                },
                btnNewOrAdd,
                btnUpdate,
                btnDelete,
                null,
                cboSearch,
                txtSearch
        );
        addBackListener(btnBack);
    }

    @Override
    protected <T, R extends Record> TableField<R, T> getPrimaryField() {
        return (TableField<R, T>) PUBLISHERS.ID;
    }

    @Override
    protected Result<Record> refreshTableAndModel(DefaultTableModel tableModel) {
        Result<Record> model = database.context()
                .select()
                .from(PUBLISHERS)
                .where(searchMapper.get(Objects.requireNonNull(cboSearch.getSelectedItem()).toString()).like("%" + txtSearch.getText() + "%"))
                .fetch();
        tableModel.setRowCount(0);
        for(Record publisher : model){
            tableModel.addRow(new String[] {
                    publisher.getValue(PUBLISHERS.ID).toString(),
                    publisher.getValue(PUBLISHERS.NAME),
                    publisher.getValue(PUBLISHERS.ADDRESS),
                    publisher.getValue(PUBLISHERS.CONTACT_NUMBER)
            });
        }
        return model;
    }

    public void clearControl(){
        ControlUtils.clearControls(txtName, txtAddress, txtContact);
    }

    public void toggleControls(boolean enabled){
        ControlUtils.toggleControl(enabled, txtName, txtAddress, txtContact);
    }

    @Override
    protected boolean validateControls() {
        return ControlUtils.validateControl(PublisherForm.this,
                new ValidationWrapper(txtName, component -> !"".equals(((JTextField)component).getText()), "Name can't be empty"),
                new ValidationWrapper(txtAddress, component -> !"".equals(((JTextField) component).getText()), "Address can't be empty"),
                new ValidationWrapper(txtContact, component -> !"".equals(((JTextField) component).getText()), "Contact can't be empty")
        );
    }

    @Override
    protected void setContent(Record record) {
        txtName.setText((record.getValue(PUBLISHERS.NAME)));
        txtAddress.setText(record.getValue(PUBLISHERS.ADDRESS));
        txtContact.setText(record.getValue(PUBLISHERS.CONTACT_NUMBER));
    }

    @Override
    protected Runnable getAddItemAction() {
        return ()-> Routines.addNewPublisher(
                database.context().configuration(),
                txtName.getText(),
                txtAddress.getText(),
                txtContact.getText()
        );
    }

    @Override
    protected Runnable getUpdateItemAction() {
        return () -> Routines.updatePublisher(
                database.context().configuration(),
                Integer.valueOf(getColumnOfSelectedRow(tblPublisher,0).toString()),
                txtName.getText(),
                txtAddress.getText(),
                txtContact.getText()
        );
    }

    @Override
    protected Runnable getDeleteItemAction() {
        return () -> Routines.removePublisher(
                database.context().configuration(),
                Integer.valueOf(getColumnOfSelectedRow(tblPublisher, 0).toString())
        );
    }

    protected void createUIComponents(){
        tblPublisher = new JTable(){
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        tblPublisher.getTableHeader().setReorderingAllowed(false);
    }
}
