package Admin;

import Admin.Report.ReportForm;
import Utils.DialogUtils;
import Utils.FormSwitching;
import Utils.SmartJFrame;
import org.jooq.Record1;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import static Admin.Application.Main.activeUserId;
import static Admin.Application.Main.database;
import static Database.generated.tables.Employees.EMPLOYEES;

public class Dashboard extends SmartJFrame {
    private JPanel view;
    private JLabel lblActiveUser;
    private JLabel btnLogout;
    private JLabel btnPublisher;
    private JLabel lblItem;
    private JLabel lblEmployee;
    private JLabel lblMember;
    private JLabel lblTransaction;
    private JLabel lblReport;

    public Dashboard(Component parent) {
        super(parent);
        initializeForm();
        setTitle("Dashboard Form");
        setContentPane(view);
        setSize(740,520);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setLocationRelativeTo(parent);
        setResizable(false);
        setVisible(true);
    }

    @Override
    public void setVisible(boolean b) {
        super.setVisible(b);
        if (b) refresh();
    }

    public void refresh() {
        Record1<String> result = database
                .context()
                .select(EMPLOYEES.NAME)
                .from(EMPLOYEES)
                .where(EMPLOYEES.ID.eq(Integer.valueOf(activeUserId)))
                .fetchOne();
        if (null != result) lblActiveUser.setText("  " + result.getValue(0));
    }

    public void initializeForm() {
        refresh();
        btnLogout.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                DialogUtils.yesNoDialog(
                        Dashboard.this,
                        "Are you sure to log out?",
                        "Confirm",
                        () -> {
                            parent.setVisible(true);
                            dispose();
                        }, null
                );
            }
        });
        btnPublisher.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                FormSwitching.switchForm(Dashboard.this, PublisherForm.class);
            }
        });

        lblItem.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                FormSwitching.switchForm(Dashboard.this, ItemCategory.class);
            }
        });

        lblEmployee.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                FormSwitching.switchForm(Dashboard.this, EmployeeForm.class);
            }
        });

        lblMember.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                FormSwitching.switchForm(Dashboard.this, MemberForm.class);
            }
        });

        lblTransaction.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                FormSwitching.switchForm(Dashboard.this, TransactionDetailForm.class);
            }
        });

        lblReport.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                FormSwitching.switchForm(Dashboard.this, ReportForm.class);
            }
        });

    }
}
