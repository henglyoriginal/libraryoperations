package Admin.BuyTransaction;

import Utils.SmartJFrame;
import org.jooq.Record;
import org.jooq.Result;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;

import static Admin.Application.Main.database;
import static Database.generated.tables.Authors.*;
import static Database.generated.tables.Books.*;
import static Database.generated.tables.Items.*;
import static Database.generated.tables.TransactionDetails.*;

public class BuyTransactionDetailForm extends SmartJFrame {
    private JPanel view;
    private JTable tblOrderDetail;
    private JButton btnBack;
    private Result<Record> orderDetails;
    private DefaultTableModel tbmOrderDetail;

    private int transactionNumber;

    public BuyTransactionDetailForm(Component parent, String transactionNumber) throws HeadlessException {
        super(parent);
        this.transactionNumber = Integer.valueOf(transactionNumber);
        initializeForm();
        setTitle("Order Detail");
        setContentPane(view);
        setSize(800,600);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setLocationRelativeTo(parent);
        setResizable(false);
        setVisible(true);
    }

    @Override
    public void initializeForm() {
        tbmOrderDetail = new DefaultTableModel();

        tblOrderDetail.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        for (String columnName : new String[]{
                "Title",
                "Author",
                "Qty",
                "Item Price",
                "Total Amount"
        }) {
            tbmOrderDetail.addColumn(columnName);
        }
        tblOrderDetail.setModel(tbmOrderDetail);

        orderDetails = database.context()
                .select()
                .from(TRANSACTION_DETAILS)
                .innerJoin(ITEMS)
                .on(TRANSACTION_DETAILS.ITEM_CODE.eq(ITEMS.ITEM_CODE))
                .innerJoin(BOOKS)
                .on(TRANSACTION_DETAILS.ITEM_CODE.eq(BOOKS.ITEM_CODE))
                .innerJoin(AUTHORS)
                .on(BOOKS.AUTHOR_ID.eq(AUTHORS.ID))
                .where(TRANSACTION_DETAILS.TRANSACTION_NUMBER.eq(transactionNumber))
                .fetch();
        for (Record orderDetail : orderDetails) {
            tbmOrderDetail.addRow(new String[] {
                    orderDetail.getValue(ITEMS.TITLE),
                    orderDetail.getValue(AUTHORS.NAME),
                    orderDetail.getValue(TRANSACTION_DETAILS.QUANTITY).toString(),
                    "$" + orderDetail.getValue(TRANSACTION_DETAILS.ITEM_PRICE),
                    "$" + orderDetail.getValue(TRANSACTION_DETAILS.TOTAL_AMOUNT)
            });
        }

        addBackListener(btnBack);
    }

    private void createUIComponents() {
        tblOrderDetail = new JTable() {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        tblOrderDetail.getTableHeader().setReorderingAllowed(false);
    }
}
