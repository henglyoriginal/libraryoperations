package Admin.BuyTransaction;

import Admin.Application.Main;
import Database.generated.Routines;
import Utils.ComboItem;
import Utils.DialogUtils;
import Utils.PopulateUtils;
import Utils.SmartJFrame;
import com.sun.javaws.exceptions.InvalidArgumentException;
import org.jooq.Record;
import org.jooq.Result;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;

import static Admin.Application.Main.database;
import static Database.generated.tables.Authors.AUTHORS;
import static Database.generated.tables.Books.BOOKS;
import static Database.generated.tables.DivisionSubjects.DIVISION_SUBJECTS;
import static Database.generated.tables.Items.ITEMS;
import static Database.generated.tables.Subjects.SUBJECTS;

public class BuyTransactionNewForm extends SmartJFrame {
    private JPanel view;
    private JTable tblBook;
    private JComboBox cboPublisher;
    private JButton btnAdd;
    private JButton btnClear;
    private JButton btnOrder;
    private JTextField txtSearch;
    private JButton btnBack;
    private DefaultTableModel tbmBook;
    private Result<Record> publishers;
    private Result<Record> books;
    private List<CustomRecord> orderTemp = new ArrayList<>();

    private class CustomRecord {
        private String code;
        private Short quantity;
        private Double price;

        public CustomRecord(String code, Short quantity, Double price) {
            this.code = code;
            this.quantity = quantity;
            this.price = price;
        }
    }

    public BuyTransactionNewForm(Component parent) throws HeadlessException {
        super(parent);
        initializeForm();
        setTitle("Ordering Form");
        setContentPane(view);
        setSize(750,600);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setLocationRelativeTo(parent);
        setResizable(false);
        setVisible(true);
    }

    private void createUIComponents() {
        tblBook = new JTable() {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        tblBook.getTableHeader().setReorderingAllowed(false);
    }

    @Override
    public void initializeForm() {

        publishers = PopulateUtils.populatePublisher(cboPublisher);

        tbmBook = new DefaultTableModel();

        tblBook.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        for (String columnName : new String[]{
                "Item Code",
                "Title",
                "Subject Code",
                "Main Subject",
                "Division Subject",
                "Pub. Year",
                "Author"
        }) {
            tbmBook.addColumn(columnName);
        }

        tblBook.setModel(tbmBook);

        tblBook.getSelectionModel().addListSelectionListener(e -> {
            btnAdd.setEnabled(!tblBook.getSelectionModel().isSelectionEmpty());
        });

        if (null != txtSearch) {
            txtSearch.getDocument().addDocumentListener(new DocumentListener() {
                @Override
                public void insertUpdate(DocumentEvent e) {
                    action();
                }

                @Override
                public void removeUpdate(DocumentEvent e) {
                    action();
                }

                @Override
                public void changedUpdate(DocumentEvent e) {

                }

                private void action() {
                    refreshTableAndModel();
                }
            });
        }

        btnAdd.addActionListener(e -> {
            JTextField txtPrice = new JTextField();
            JTextField txtQuantity = new JTextField();
            final JComponent[] inputs = new JComponent[] {
                    new JLabel("Price ($)"),
                    txtPrice,
                    new JLabel("Quantity"),
                    txtQuantity
            };
            int result = JOptionPane.showConfirmDialog(BuyTransactionNewForm.this, inputs, "Detail", JOptionPane.OK_CANCEL_OPTION);
            if (result == JOptionPane.OK_OPTION) {
                String price = txtPrice.getText();
                String quantity = txtQuantity.getText();
                if (!"".equals(price) && !"".equals(quantity)) {
                    try {
                        Short quantityValue = Short.valueOf(quantity);
                        Double priceValue = Double.valueOf(price);
                        if (quantityValue <= 0 || priceValue <= 0) throw new NumberFormatException();
                        orderTemp.add(new CustomRecord(
                                getColumnOfSelectedRow(tblBook, 0).toString(),
                                quantityValue,
                                priceValue
                        ));
                        books.remove(tblBook.getSelectedRow());
                        refreshTable();
                        configButton(true, "Order (" + orderTemp.size() + ")");
                    } catch (NumberFormatException e1) {
                        DialogUtils.showErrorMsg(BuyTransactionNewForm.this, "Invalid values");
                    }
                }
            }
        });

        btnClear.addActionListener(e -> {
            orderTemp.clear();
            refreshTableAndModel();
            configButton(false, "Order");
        });

        btnOrder.addActionListener(e -> {
            if (-1 != cboPublisher.getSelectedIndex()) {
                String id = database.context().select(
                        Routines.addOrder(
                                ((ComboItem<String, Integer>) cboPublisher.getSelectedItem()).getValue(),
                                Integer.valueOf(Main.activeUserId)
                        )
                ).fetchOne().getValue(0).toString();

                for (CustomRecord customRecord : orderTemp) {
                    Routines.addOrderDetail(
                            database.context().configuration(),
                            Integer.valueOf(id),
                            customRecord.code,
                            customRecord.quantity,
                            customRecord.price
                    );
                }

                DialogUtils.showInfoMsg(BuyTransactionNewForm.this, "Successfully ordered!");
                btnClear.doClick();
            } else {
                DialogUtils.showWarningMsg(BuyTransactionNewForm.this, "Please select a publisher");
            }
        });

        refreshTableAndModel();
        addBackListener(btnBack);
    }

    private void configButton(boolean enabled, String orderText) {
        btnOrder.setText(orderText);
        btnOrder.setEnabled(enabled);
        btnClear.setEnabled(enabled);
    }

    private void refreshTableAndModel() {
        books = PopulateUtils.getBooks()
                .where(ITEMS.TITLE.like("%" + txtSearch.getText() + "%"))
                .fetch();
        List<String> allTempIds = new ArrayList<>();
        for (CustomRecord customRecord : orderTemp) {
            allTempIds.add(customRecord.code);
        }
        books.removeIf(record -> allTempIds.contains(record.getValue(ITEMS.ITEM_CODE)));
        refreshTable();
    }

    private void refreshTable() {
        tbmBook.setRowCount(0);
        for (Record book : books) {
            Short publicationYear = book.getValue(BOOKS.PUBLICATION_YEAR);
            tbmBook.addRow(new String[] {
                    book.getValue(ITEMS.ITEM_CODE),
                    book.getValue(ITEMS.TITLE),
                    book.getValue(SUBJECTS.CODE).concat(".").concat(book.getValue(DIVISION_SUBJECTS.CODE)),
                    book.getValue(SUBJECTS.NAME),
                    book.getValue(DIVISION_SUBJECTS.NAME),
                    null != publicationYear ? publicationYear.toString() : "-",
                    book.getValue(AUTHORS.NAME)
            });
        }
    }
}
