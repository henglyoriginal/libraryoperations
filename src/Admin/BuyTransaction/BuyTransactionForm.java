package Admin.BuyTransaction;

import Admin.Application.Main;
import Database.generated.Routines;
import Utils.*;
import org.jooq.Record;
import org.jooq.Result;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;
import java.awt.*;

import static Admin.Application.Main.database;
import static Database.generated.tables.BuyTransactions.BUY_TRANSACTIONS;
import static Database.generated.tables.Employees.EMPLOYEES;
import static Database.generated.tables.Publishers.PUBLISHERS;

public class BuyTransactionForm extends SmartJFrame {

    private interface Status {
        String SHIPPING = "Shipping";
        String ACCEPTED = "Accepted";
        String CANCELLED = "Cancelled";
    }

    private JPanel view;
    private JTable tblBuyTransaction;
    private DefaultTableModel tableModel;
    private JButton btnNew;
    private JButton btnAccept;
    private JButton btnCancel;
    private JButton btnDetail;
    private JButton btnBack;
    private Result<Record> model;

    public BuyTransactionForm(Component parent) throws HeadlessException {
        super(parent);
        initializeForm();
        setTitle("Buy Transaction Form");
        setContentPane(view);
        setSize(650,600);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setLocationRelativeTo(parent);
        setResizable(false);
        setVisible(true);
    }


    @Override
    public void initializeForm() {

        tableModel = new DefaultTableModel();

        tblBuyTransaction.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        for (String columnName : new String[]{
                "ID",
                "Order Date",
                "Order By",
                "Publisher",
                "Status"
        }) {
            tableModel.addColumn(columnName);
        }
        refreshTableAndModel();
        tblBuyTransaction.setModel(tableModel);
        TableColumnModel tableColumnModel = tblBuyTransaction.getColumnModel();
        tableColumnModel.removeColumn(tableColumnModel.getColumn(0));

        tblBuyTransaction.getSelectionModel().addListSelectionListener(e -> {
            controlConfiguration(!tblBuyTransaction.getSelectionModel().isSelectionEmpty());
        });

        btnNew.addActionListener(e -> FormSwitching.switchForm(BuyTransactionForm.this, BuyTransactionNewForm.class));

        btnAccept.addActionListener(e -> {
            if (AuthenticateUtils.isAuthenticated(BuyTransactionForm.this, Main.activeUsername, "your")) {
                Routines.acceptOrder(
                        database.context().configuration(),
                        Integer.valueOf(getColumnOfSelectedRow(tblBuyTransaction, 0).toString()),
                        Integer.valueOf(Main.activeUserId)
                );

                DialogUtils.showInfoMsg(BuyTransactionForm.this, "Successfully accepted!");

                refreshTableAndModel();
            }
            //TODO: add books
        });

        btnCancel.addActionListener(e -> {
            if (AuthenticateUtils.isAuthenticated(BuyTransactionForm.this, Main.activeUsername, "your")) {
                Routines.cancelOrder(
                        database.context().configuration(),
                        Integer.valueOf(getColumnOfSelectedRow(tblBuyTransaction, 0).toString()),
                        Integer.valueOf(Main.activeUserId)
                );

                DialogUtils.showInfoMsg(BuyTransactionForm.this, "Successfully cancelled!");

                refreshTableAndModel();
            }
        });

        btnDetail.addActionListener(e -> {
            FormSwitching.switchForm(BuyTransactionForm.this, BuyTransactionDetailForm.class, getColumnOfSelectedRow(tblBuyTransaction, 0).toString());
        });

        addBackListener(btnBack);
    }

    private void controlConfiguration(boolean enable) {
        btnAccept.setEnabled(enable);
        btnCancel.setEnabled(enable);
        btnDetail.setEnabled(enable);
    }

    private void refreshTableAndModel() {
        model = database.context()
                .select()
                .from(BUY_TRANSACTIONS)
                .innerJoin(PUBLISHERS)
                .on(PUBLISHERS.ID.eq(BUY_TRANSACTIONS.PUBLISHER_ID))
                .innerJoin(EMPLOYEES)
                .on(EMPLOYEES.ID.eq(BUY_TRANSACTIONS.ORDER_EMPLOYEE_ID))
                .where(BUY_TRANSACTIONS.STATUS.eq(Status.SHIPPING))
                .orderBy(BUY_TRANSACTIONS.ORDER_DATE.desc())
                .fetch();
        tableModel.setRowCount(0);
        for (Record item : model) {
            tableModel.addRow(new String[] {
                    item.getValue(BUY_TRANSACTIONS.TRANSACTION_NUMBER).toString(),
                    LocalDateTimeFormatter.format(item.getValue(BUY_TRANSACTIONS.ORDER_DATE).toLocalDateTime()),
                    item.getValue(EMPLOYEES.NAME),
                    item.getValue(PUBLISHERS.NAME),
                    item.getValue(BUY_TRANSACTIONS.STATUS)
            });
        }
    }

    @Override
    public void setVisible(boolean b) {
        super.setVisible(b);
        if (b) refreshTableAndModel();
    }

    private void createUIComponents() {
        tblBuyTransaction = new JTable() {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        tblBuyTransaction.getTableHeader().setReorderingAllowed(false);
    }
}
