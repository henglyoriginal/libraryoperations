package Admin;

import Admin.Items.*;
import Utils.FormSwitching;
import Utils.SmartJFrame;
import com.sun.istack.internal.NotNull;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class ItemCategory extends SmartJFrame {
    private JPanel view;
    private JLabel btnBack;
    private JLabel lblConferenceProceeding;
    private JLabel lblDVD;
    private JLabel lblBook;
    private JLabel lblJournal;
    private JLabel lblReferenceTextBook;

    public ItemCategory(@NotNull Component parent) {
        super(parent);
        initializeForm();
        setContentPane(view);
        setSize(parent.getSize());
        setLocationRelativeTo(parent);
        setResizable(false);
        setVisible(true);
        setTitle("Item Category");
    }

    public void initializeForm(){
        btnBack.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (null != parent) parent.setVisible(true);
                dispose();
            }
        });

        lblConferenceProceeding.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                FormSwitching.switchForm(ItemCategory.this, ConferenceProceedingForm.class);
            }
        });

        lblDVD.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                FormSwitching.switchForm(ItemCategory.this, DVDForm.class);
            }
        });

        lblBook.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                FormSwitching.switchForm(ItemCategory.this, BookForm.class);
            }
        });

        lblJournal.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                FormSwitching.switchForm(ItemCategory.this, JournalForm.class);
            }
        });

        lblReferenceTextBook.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                FormSwitching.switchForm(ItemCategory.this, ReferenceTextbook.class);
            }
        });
    }
}
