package Admin.Report;

import Database.generated.tables.BuyTransactions;
import Database.generated.tables.Publishers;
import Database.generated.tables.TransactionDetails;
import Utils.CustomIdName;
import Utils.PopulateUtils;
import Utils.SmartReportJFrame;
import Utils.SmartTimeReportJFrame;
import org.jooq.Result;
import org.jooq.impl.DSL;

import javax.swing.*;
import java.awt.*;
import java.util.Map;

import static Admin.Application.Main.database;
import static Database.generated.tables.BuyTransactions.*;
import static Database.generated.tables.Publishers.*;
import static Database.generated.tables.TransactionDetails.*;

public class ExpenseForm extends SmartTimeReportJFrame {
    private JPanel view;
    private JComboBox cboYear;
    private JComboBox cboMonth;
    private JTable tblData;
    private JButton btnExport;
    private JButton btnBack;
    private JComboBox cboPreference;

    public ExpenseForm(Component parent) throws HeadlessException {
        super(parent);
        initializeForm();
        setTitle("Expense Report");
        setContentPane(view);
        setSize(800,600);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setLocationRelativeTo(parent);
        setResizable(false);
        setVisible(true);
    }

    @Override
    protected Runnable getYearPopulator() {
        return () -> PopulateUtils.populateExpenseYear(cboYear);
    }

    @Override
    protected Runnable getMonthPopulator() {
        return () -> PopulateUtils.populateExpenseMonth(cboMonth, getSelectedYear());
    }

    @Override
    protected Map<String, Object> getParameterValues() {
        Map<String, Object> parameterValues = super.getParameterValues();
        parameterValues.put("year", getSelectedYear());
        parameterValues.put("month", MONTHLY.equals(getSelectedPreference()) ? getSelectedMonth() : Integer.valueOf(0));
        return parameterValues;
    }

    @Override
    protected String getReportName() {
        return "Expense";
    }

    @Override
    protected Result<?> readData() {
        return database.context()
                .select(PUBLISHERS.NAME.as("Name"), PUBLISHERS.ADDRESS.as("Address"), PUBLISHERS.CONTACT_NUMBER.as("Contact"), DSL.sum(TRANSACTION_DETAILS.TOTAL_AMOUNT).as("Total Amount ($)"))
                .from(PUBLISHERS, TRANSACTION_DETAILS, BUY_TRANSACTIONS)
                .where(
                        BUY_TRANSACTIONS.PUBLISHER_ID.eq(PUBLISHERS.ID)
                        .and(BUY_TRANSACTIONS.TRANSACTION_NUMBER.eq(TRANSACTION_DETAILS.TRANSACTION_NUMBER))
                        .and(BUY_TRANSACTIONS.RECEIVING_DATE.isNotNull())
                        .and(DSL.year(BUY_TRANSACTIONS.RECEIVING_DATE).eq(getSelectedYear()))
                        .and(MONTHLY.equals(getSelectedPreference()) ? DSL.month(BUY_TRANSACTIONS.RECEIVING_DATE).eq(getSelectedMonth()) : DSL.trueCondition())
                )
                .groupBy(PUBLISHERS.ID, PUBLISHERS.NAME)
                .orderBy(DSL.sum(TRANSACTION_DETAILS.TOTAL_AMOUNT).desc())
                .fetch();
    }

    @Override
    public void initializeForm() {
        initialize(tblData, btnExport, cboYear, cboMonth, cboPreference);
        addBackListener(btnBack);
    }

    private void createUIComponents() {
        tblData = new JTable() {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        tblData.getTableHeader().setReorderingAllowed(false);
    }

}
