package Admin.Report;

import Utils.PopulateUtils;
import Utils.SmartTimeReportJFrame;
import org.jooq.Result;
import org.jooq.impl.DSL;

import javax.swing.*;
import java.awt.*;
import java.util.Map;

import static Admin.Application.Main.database;
import static Database.generated.tables.Histories.HISTORIES;
import static Database.generated.tables.Members.MEMBERS;

public class OutstandingMemberForm extends SmartTimeReportJFrame {
    private JPanel view;
    private JButton btnExport;
    private JButton btnBack;
    private JComboBox cboYear;
    private JComboBox cboMonth;
    private JTable tblData;

    @Override
    protected Runnable getYearPopulator() {
        return () -> PopulateUtils.populateRevenueForOutstandingYear(cboYear);
    }

    @Override
    protected Runnable getMonthPopulator() {
        return () -> PopulateUtils.populateRevenueForOutstandingMonth(cboMonth, getSelectedYear());
    }

    public OutstandingMemberForm(Component parent) throws HeadlessException {
        super(parent);
        initializeForm();
        setTitle("Outstanding Member");
        setContentPane(view);
        setSize(800,600);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setLocationRelativeTo(parent);
        setResizable(false);
        setVisible(true);
    }


    @Override
    protected String getReportName() {
        return "OutstandingMember";
    }

    @Override
    protected Map<String, Object> getParameterValues() {
        Map<String, Object> parameterValues = super.getParameterValues();
        parameterValues.put("year", getSelectedYear());
        parameterValues.put("month", getSelectedMonth());
        return parameterValues;
    }

    @Override
    public void initializeForm() {
        initialize(tblData, btnExport, cboYear, cboMonth);
        addBackListener(btnBack);
    }

    @Override
    protected Result<?> readData() {
        return database.context()
                .select(
                        MEMBERS.NAME.as("Name"),
                        MEMBERS.SEX.as("Sex"),
                        MEMBERS.REGISTRATION_DATE.as("Reg. Date"),
                        DSL.ifnull(DSL.sum(HISTORIES.CHARGED_AMOUNT), 0).as("Charge Amount ($)")
                )
                .from(MEMBERS, HISTORIES)
                .where(
                        HISTORIES.MEMBER_ID.eq(MEMBERS.ID)
                                .and(HISTORIES.RETURN_DATE.isNotNull())
                                .and(DSL.year(HISTORIES.RETURN_DATE).eq(getSelectedYear())
                                        .and(DSL.month(HISTORIES.RETURN_DATE).eq(getSelectedMonth())))
                )
                .groupBy(MEMBERS.NAME, MEMBERS.SEX, MEMBERS.ID)
                .orderBy(DSL.sum(HISTORIES.CHARGED_AMOUNT))
                .limit(15)
                .fetch();
    }

    private void createUIComponents() {
        tblData = new JTable() {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        tblData.getTableHeader().setReorderingAllowed(false);
    }

}
