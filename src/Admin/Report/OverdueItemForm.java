package Admin.Report;

import Utils.*;
import org.jooq.Record;
import org.jooq.Record2;
import org.jooq.Record4;
import org.jooq.Result;

import javax.swing.*;
import java.awt.*;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Map;

import static Admin.Application.Main.database;
import static Database.generated.tables.Histories.*;
import static Database.generated.tables.ItemCategories.*;
import static Database.generated.tables.ItemDetails.*;
import static Database.generated.tables.Items.*;
import static Database.generated.tables.MemberCategories.*;
import static Database.generated.tables.Members.*;

public class OverdueItemForm extends SmartReportJFrame {
    private JPanel view;
    private JComboBox cboOverdueMember;
    private JTable tblData;
    private JButton btnExport;
    private JButton btnBack;
    private Result<Record> overdueMembers;
    private Result<Record4<String, Timestamp, Timestamp, String>> data;

    @Override
    protected String getReportName() {
        return "Overdue";
    }

    @Override
    protected Map<String, Object> getParameterValues() {
        Map<String, Object> parameterValues = super.getParameterValues();
        Integer id = ((ComboItem<String, Integer>) cboOverdueMember.getSelectedItem()).getValue();
        Record2<String, String> member = database.context()
                .select(MEMBERS.NAME, MEMBERS.ADDRESS)
                .from(MEMBERS)
                .where(MEMBERS.ID.eq(id))
                .fetchOne();
        parameterValues.put("member_id", new CustomIdName(id, member.getValue(MEMBERS.NAME)));
        parameterValues.put("member_address", new CustomIdName(id, member.getValue(MEMBERS.ADDRESS)));
        return parameterValues;
    }

    public OverdueItemForm(Component parent) throws HeadlessException {
        super(parent);
        initializeForm();
        setTitle("Poor Quality Item");
        setContentPane(view);
        setSize(800,600);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setLocationRelativeTo(parent);
        setResizable(false);
        setVisible(true);
    }

    @Override
    public void initializeForm() {
        overdueMembers = PopulateUtils.populateOverdueMember(cboOverdueMember);

        initialize(tblData, btnExport);

        cboOverdueMember.addItemListener(e -> {
            refreshData();
            refreshTable();
        });

        addBackListener(btnBack);
    }

    @Override
    protected Result<?> readData() {
        return data = database.context()
                .select(ITEMS.TITLE.as("Title"), HISTORIES.BORROWED_DATE.as("Borrow Date"), HISTORIES.DUE_DATE.as("Due Date"), ITEM_CATEGORIES.NAME.as("Type"))
                .from(ITEMS, HISTORIES, ITEM_CATEGORIES, MEMBERS, MEMBER_CATEGORIES, ITEM_DETAILS)
                .where(
                        MEMBERS.CATEGORY_ID.eq(MEMBER_CATEGORIES.ID)
                                .and(HISTORIES.MEMBER_ID.eq(MEMBERS.ID))
                                .and(HISTORIES.ITEM_DETAIL_ID.eq(ITEM_DETAILS.ID))
                                .and(ITEM_DETAILS.ITEM_CODE.eq(ITEMS.ITEM_CODE))
                                .and(ITEMS.CATEGORY_ID.eq(ITEM_CATEGORIES.ID))
                                .and(HISTORIES.RETURN_DATE.isNull())
                                .and(HISTORIES.DUE_DATE.lt(Timestamp.valueOf(LocalDateTime.now())))
                                .and(MEMBERS.ID.eq(((ComboItem<String, Integer>) cboOverdueMember.getSelectedItem()).getValue()))
                )
                .fetch();
    }

    private void createUIComponents() {
        tblData = new JTable() {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        tblData.getTableHeader().setReorderingAllowed(false);
    }
}
