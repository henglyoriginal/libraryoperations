package Admin.Report;

import Database.generated.Routines;
import Utils.ControlUtils;
import Utils.FormSwitching;
import Utils.SmartJFrame;
import org.jooq.Configuration;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import static Admin.Application.Main.database;

public class ReportForm extends SmartJFrame {
    private JPanel view;
    private JLabel lblPoorItem;
    private JLabel lblOverdueItem;
    private JLabel lblOutstandingMember;
    private JLabel lblExpense;
    private JLabel btnBack;
    private JLabel lblRevenue;

    public ReportForm(Component parent) throws HeadlessException {
        super(parent);
        initializeForm();
        setTitle("Report");
        setContentPane(view);
        setSize(parent.getSize());
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setLocationRelativeTo(parent);
        setResizable(false);
        setVisible(true);
    }

    @Override
    public void initializeForm() {

        Configuration configuration = database.context().configuration();

        ControlUtils.controlEnabler(
                new ControlUtils.ControlWrapper(lblPoorItem, () -> Routines.countPoorItems(configuration)),
                new ControlUtils.ControlWrapper(lblOverdueItem, () -> Routines.countOverdueItems(configuration)),
                new ControlUtils.ControlWrapper(lblOutstandingMember, () -> Routines.countOutstandingMembers(configuration)),
                new ControlUtils.ControlWrapper(lblExpense, () -> Routines.countExpenses(configuration)),
                new ControlUtils.ControlWrapper(lblRevenue, () -> Routines.countRevenues(configuration))
        );

        lblPoorItem.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (lblPoorItem.isEnabled()) FormSwitching.switchForm(ReportForm.this, PoorItemForm.class);
            }
        });

        lblOverdueItem.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (lblOverdueItem.isEnabled()) FormSwitching.switchForm(ReportForm.this, OverdueItemForm.class);
            }
        });

        lblOutstandingMember.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (lblOutstandingMember.isEnabled()) FormSwitching.switchForm(ReportForm.this, OutstandingMemberForm.class);
            }
        });

        lblExpense.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (lblExpense.isEnabled()) FormSwitching.switchForm(ReportForm.this, ExpenseForm.class);
            }
        });

        lblRevenue.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (lblRevenue.isEnabled()) FormSwitching.switchForm(ReportForm.this, RevenueForm.class);
            }
        });

        addBackListener(btnBack);
    }
}
