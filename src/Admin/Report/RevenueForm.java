package Admin.Report;

import Utils.PopulateUtils;
import Utils.SmartTimeReportJFrame;
import org.jooq.Result;
import org.jooq.impl.DSL;

import javax.swing.*;
import java.awt.*;
import java.util.Map;

import static Admin.Application.Main.database;
import static Database.generated.tables.Histories.HISTORIES;
import static Database.generated.tables.ItemDetails.ITEM_DETAILS;
import static Database.generated.tables.Items.ITEMS;

public class RevenueForm extends SmartTimeReportJFrame {
    private JPanel view;
    private JComboBox cboYear;
    private JComboBox cboMonth;
    private JComboBox cboPreference;
    private JTable tblData;
    private JButton btnExport;
    private JButton btnBack;

    public RevenueForm(Component parent) throws HeadlessException {
        super(parent);
        initializeForm();
        setTitle("Revenue Report");
        setContentPane(view);
        setSize(800,600);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setLocationRelativeTo(parent);
        setResizable(false);
        setVisible(true);
    }

    @Override
    protected Runnable getYearPopulator() {
        return () -> PopulateUtils.populateRevenueYear(cboYear);
    }

    @Override
    protected Runnable getMonthPopulator() {
        return () -> PopulateUtils.populateRevenueMonth(cboMonth, getSelectedYear());
    }

    @Override
    protected Map<String, Object> getParameterValues() {
        Map<String, Object> parameterValues = super.getParameterValues();
        parameterValues.put("year", getSelectedYear());
        parameterValues.put("month", MONTHLY.equals(getSelectedPreference()) ? getSelectedMonth() : Integer.valueOf(0));
        return parameterValues;
    }

    @Override
    protected String getReportName() {
        return "Revenue";
    }

    @Override
    protected Result<?> readData() {
        return database.context()
                .select(ITEMS.TITLE.as("Title"), DSL.sum(HISTORIES.CHARGED_AMOUNT).as("Charged Amount ($)"))
                .from(ITEMS, HISTORIES, ITEM_DETAILS)
                .where(
                        HISTORIES.ITEM_DETAIL_ID.eq(ITEM_DETAILS.ID)
                        .and(ITEM_DETAILS.ITEM_CODE.eq(ITEMS.ITEM_CODE))
                        .and(HISTORIES.RETURN_DATE.isNotNull())
                        .and(DSL.year(HISTORIES.RETURN_DATE).eq(getSelectedYear()))
                        .and(HISTORIES.CHARGED_AMOUNT.gt(Short.valueOf("0")))
                        .and(MONTHLY.equals(getSelectedPreference()) ? DSL.month(HISTORIES.RETURN_DATE).eq(getSelectedMonth()) : DSL.trueCondition())
                )
                .groupBy(ITEMS.ITEM_CODE, ITEMS.TITLE)
                .orderBy(DSL.sum(HISTORIES.CHARGED_AMOUNT).desc())
                .fetch();
    }

    @Override
    public void initializeForm() {
        initialize(tblData, btnExport, cboYear, cboMonth, cboPreference);
        addBackListener(btnBack);
    }

    private void createUIComponents() {
        tblData = new JTable() {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        tblData.getTableHeader().setReorderingAllowed(false);
    }
}
