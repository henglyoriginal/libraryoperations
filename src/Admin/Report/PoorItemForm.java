package Admin.Report;

import Utils.ReportUtils;
import Utils.SmartJFrame;
import Utils.SmartReportJFrame;
import org.jooq.Record;
import org.jooq.Record3;
import org.jooq.Result;
import ro.nextreports.engine.FluentReportRunner;
import ro.nextreports.engine.Report;

import javax.swing.*;
import javax.swing.table.AbstractTableModel;
import java.awt.*;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

import static Admin.Application.Main.database;
import static Database.generated.tables.ItemDetails.ITEM_DETAILS;
import static Database.generated.tables.Items.ITEMS;

public class PoorItemForm extends SmartReportJFrame {
    private JPanel view;
    private JTable tblData;
    private JButton btnExport;
    private JButton btnBack;

    @Override
    protected String getReportName() {
        return "PoorItem";
    }

    public PoorItemForm(Component parent) throws HeadlessException {
        super(parent);
        initializeForm();
        setTitle("Poor Quality Item");
        setContentPane(view);
        setSize(800,600);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setLocationRelativeTo(parent);
        setResizable(false);
        setVisible(true);
    }

    @Override
    public void initializeForm() {

        initialize(tblData, btnExport);

        addBackListener(btnBack);
    }

    @Override
    protected Result<?> readData() {
        return database.context()
                .select(ITEMS.TITLE.as("Title"), ITEMS.ITEM_CODE.as("Code"), ITEM_DETAILS.ITEM_SUB_CODE.as("Sub Code"))
                .from(ITEMS, ITEM_DETAILS)
                .where(ITEM_DETAILS.ITEM_CODE.eq(ITEMS.ITEM_CODE).and(ITEM_DETAILS.CONDITION.eq("Poor")))
                .fetch();
    }

    private void createUIComponents() {
        tblData = new JTable() {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        tblData.getTableHeader().setReorderingAllowed(false);
    }
}
