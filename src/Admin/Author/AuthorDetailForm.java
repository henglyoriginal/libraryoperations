package Admin.Author;

import Utils.PopulateUtils;
import Utils.SmartJFrame;
import org.jooq.Field;
import org.jooq.Record;
import org.jooq.Result;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ItemEvent;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import static Database.generated.tables.Authors.AUTHORS;
import static Database.generated.tables.Books.BOOKS;
import static Database.generated.tables.DivisionSubjects.DIVISION_SUBJECTS;
import static Database.generated.tables.Items.ITEMS;
import static Database.generated.tables.Subjects.SUBJECTS;

public class AuthorDetailForm extends SmartJFrame {
    private JPanel view;
    private JComboBox cboSearch;
    private JTextField txtSearch;
    private JLabel lblPrefix;
    private JTable tblBook;
    private JButton btnBack;
    private Integer authorId;
    private Result<Record> books;
    private DefaultTableModel tbmBook;
    protected Map<String, Field<String>> searchMapper = new HashMap<>();

    public AuthorDetailForm(Component parent, String authorId) throws HeadlessException {
        super(parent);
        this.authorId = Integer.valueOf(authorId);
        initializeForm();
        setTitle("Author Detail");
        setContentPane(view);
        setSize(800,600);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setLocationRelativeTo(parent);
        setResizable(false);
        setVisible(true);
    }

    @Override
    public void initializeForm() {

        searchMapper.put("Item Code", BOOKS.ITEM_CODE);
        searchMapper.put("Title", ITEMS.TITLE);
        searchMapper.put("ISBN", BOOKS.ISBN);
        searchMapper.put("Keyword", BOOKS.KEYWORD);

        tbmBook = new DefaultTableModel();

        tblBook.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        tbmBook.addColumn("Item Code");
        tbmBook.addColumn("Title");
        tbmBook.addColumn("Subject Code");
        tbmBook.addColumn("Main Subject");
        tbmBook.addColumn("Division Subject");
        tbmBook.addColumn("Pub. Year");

        tblBook.setModel(tbmBook);

        if (null != txtSearch) {
            txtSearch.getDocument().addDocumentListener(new DocumentListener() {
                @Override
                public void insertUpdate(DocumentEvent e) {
                    action();
                }

                @Override
                public void removeUpdate(DocumentEvent e) {
                    action();
                }

                @Override
                public void changedUpdate(DocumentEvent e) {

                }

                private void action() {
                    refreshTableAndModel();
                }
            });
        }

        if (null != cboSearch) {
            cboSearch.addItemListener(e -> {
                if (e.getStateChange() == ItemEvent.SELECTED) {
                    lblPrefix.setText(cboSearch.getSelectedItem().toString().equals("Item Code") ? ( "BK-") : "   ");
                    refreshTableAndModel();
                }
            });
        }

        refreshTableAndModel();

        addBackListener(btnBack);
    }

    private void refreshTableAndModel() {
        Field<String> mapperValue = searchMapper.get(Objects.requireNonNull(cboSearch.getSelectedItem()).toString());
        String searchValue = txtSearch.getText();
        books = PopulateUtils.getBooks()
                .where(AUTHORS.ID.eq(authorId).and(ITEMS.CATEGORY_ID.eq(Byte.valueOf("1")).and(mapperValue == BOOKS.ITEM_CODE && !"".equals(searchValue) ? mapperValue.eq("BK-" + searchValue) : mapperValue.like("%" + searchValue + "%"))))
                .fetch();
        tbmBook.setRowCount(0);
        for (Record book : books) {
            Short publicationYear = book.getValue(BOOKS.PUBLICATION_YEAR);
            tbmBook.addRow(new String[] {
                    book.getValue(ITEMS.ITEM_CODE),
                    book.getValue(ITEMS.TITLE),
                    book.getValue(SUBJECTS.CODE).concat(".").concat(book.getValue(DIVISION_SUBJECTS.CODE)),
                    book.getValue(SUBJECTS.NAME),
                    book.getValue(DIVISION_SUBJECTS.NAME),
                    null != publicationYear ? publicationYear.toString() : "-"
            });
        }
    }

    private void createUIComponents() {
        tblBook = new JTable() {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        tblBook.getTableHeader().setReorderingAllowed(false);
    }
}
