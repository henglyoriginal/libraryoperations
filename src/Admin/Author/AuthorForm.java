package Admin.Author;

import Database.generated.Routines;
import Utils.*;
import org.jooq.Record;
import org.jooq.Result;
import org.jooq.TableField;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.util.Objects;

import static Admin.Application.Main.database;
import static Database.generated.tables.Authors.AUTHORS;

public class AuthorForm extends SmartCRUDJFrame {
    private JPanel view;
    private JTextField txtName;
    private JTextField txtAddress;
    private JRadioButton rbFemale;
    private JRadioButton rbMale;
    private JButton btnNewOrAdd;
    private JButton btnUpdate;
    private JButton btnDelete;
    private JButton btnBack;
    private JComboBox cboSearch;
    private JTextField txtSearch;
    private JTable tblAuthor;
    private JButton btnDetail;
    private ButtonGroup sexGroup;

    public AuthorForm(Component parent) throws HeadlessException {
        super(parent);
        initializeForm();
        setTitle("Author Form");
        setContentPane(view);
        setSize(500,600);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setLocationRelativeTo(parent);
        setResizable(false);
        setVisible(true);
    }

    @Override
    public void initializeForm() {
        sexGroup = new ButtonGroup();
        sexGroup.add(rbFemale);
        sexGroup.add(rbMale);

        initializeTableComponents(
                tblAuthor,
                new String[] {
                        "ID",
                        "Name",
                        "Sex",
                        "Address"
                },
                new int[] {0},
                new ValueWrapper[] {
                        new ValueWrapper("Name", AUTHORS.NAME),
                        new ValueWrapper("Address", AUTHORS.ADDRESS)
                },
                btnNewOrAdd,
                btnUpdate,
                btnDelete,
                btnDetail,
                cboSearch,
                txtSearch
        );

        addBackListener(btnBack);
    }

    @Override
    protected <T, R extends Record> TableField<R, T> getPrimaryField() {
        return (TableField<R, T>) AUTHORS.ID;
    }

    @Override
    protected Result<Record> refreshTableAndModel(DefaultTableModel tableModel) {
        Result<Record> model = database.context()
                .select()
                .from(AUTHORS)
                .where(searchMapper.get(Objects.requireNonNull(cboSearch.getSelectedItem()).toString()).like("%" + txtSearch.getText() + "%"))
                .orderBy(AUTHORS.NAME.asc())
                .fetch();
        tableModel.setRowCount(0);
        for (Record employee : model) {
            tableModel.addRow(new String[] {
                    employee.getValue(AUTHORS.ID).toString(),
                    employee.getValue(AUTHORS.NAME),
                    employee.getValue(AUTHORS.SEX),
                    employee.getValue(AUTHORS.ADDRESS)
            });
        }
        return model;
    }

    @Override
    protected void clearControl() {
        ControlUtils.clearControls(
                txtName,
                txtAddress
        );
        rbFemale.setSelected(true);
    }

    @Override
    protected void toggleControls(boolean enabled) {
        ControlUtils.toggleControl(
                enabled,
                txtName,
                rbFemale,
                rbMale,
                txtAddress
        );
    }

    @Override
    protected boolean validateControls() {
        return ControlUtils.validateControl(AuthorForm.this,
                new ValidationWrapper(txtName, component -> !"".equals(((JTextField)component).getText()), "Name can't be empty"),
                new ValidationWrapper(txtAddress, component -> !"".equals(((JTextField) component).getText()), "Address can't be empty"));
    }

    @Override
    protected void setContent(Record record) {
        String sex = record.getValue(AUTHORS.SEX);
        txtName.setText(record.getValue(AUTHORS.NAME));
        if (null == sex || "F".equals(sex)) rbFemale.setSelected(true);
        else rbMale.setSelected(true);
        txtAddress.setText(record.getValue(AUTHORS.ADDRESS));
    }

    @Override
    protected Runnable getAddItemAction() {
        return () -> Routines.addAuthor(
                database.context().configuration(),
                txtName.getText(),
                rbFemale.isSelected() ? "F" : "M",
                txtAddress.getText()
        );
    }

    @Override
    protected Runnable getUpdateItemAction() {
        return () -> Routines.updateAuthor(
                database.context().configuration(),
                Integer.valueOf(getColumnOfSelectedRow(tblAuthor, 0).toString()),
                txtName.getText(),
                rbFemale.isSelected() ? "F" : "M",
                txtAddress.getText()
        );
    }

    @Override
    protected Runnable getDeleteItemAction() {
        return () -> Routines.deleteAuthor(
                database.context().configuration(),
                Integer.valueOf(getColumnOfSelectedRow(tblAuthor, 0).toString())
        );
    }

    @Override
    protected Runnable getDetailAction() {
        return () -> {
            FormSwitching.switchForm(AuthorForm.this, AuthorDetailForm.class, getColumnOfSelectedRow(tblAuthor, 0).toString());
        };
    }


    private void createUIComponents() {
        tblAuthor = new JTable() {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        tblAuthor.getTableHeader().setReorderingAllowed(false);
    }
}
