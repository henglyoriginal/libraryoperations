package Admin.Authentication;

import Admin.Common.EmployeeAddListener;
import Utils.*;

import javax.swing.*;
import java.awt.*;

public class SignUpForm extends SmartJFrame {
    private JPanel view;
    private JTextField txtName;
    private JTextField txtUsername;
    private JButton btnSignUp;
    private JButton btnBack;
    private JTextField txtAddress;
    private JPasswordField txtPassword;
    private JPasswordField txtConfirmPassword;
    private JPanel datePickerLayout;
    private JComboBox cboEducation;
    private JRadioButton rbFemale;
    private JRadioButton rbMale;
    private JTextField txtContactNumber;
    private SmartDatePicker datePicker;
    private ButtonGroup sexGroup;

    public SignUpForm(Component parent) {
        super(parent);
        initializeForm();
        setTitle("SignUp");
        setContentPane(view);
        setSize(400,420);
        setLocationRelativeTo(parent);
        setResizable(false);
        setVisible(true);
    }

    public void initializeForm() {
        SmartFontDatePickerSettings datePickerSettings = new SmartFontDatePickerSettings(
                new Font("Google Sans", Font.PLAIN, 16)
        );
        datePicker = new SmartDatePicker(datePickerSettings);
        datePickerLayout.add(datePicker, "Center");

        PopulateUtils.populateEducation(cboEducation);

        sexGroup = new ButtonGroup();
        sexGroup.add(rbFemale);
        sexGroup.add(rbMale);

        btnSignUp.addActionListener(new EmployeeAddListener(
                parent,
                txtName,
                rbFemale,
                txtAddress,
                datePicker,
                txtContactNumber,
                txtUsername,
                txtPassword,
                txtConfirmPassword,
                cboEducation,
                () -> {
                    DialogUtils.showInfoMsg(parent, "Successfully signed up.");
                    ControlUtils.clearControls(
                            txtName,
                            txtAddress,
                            datePicker.getComponentDateTextField(),
                            txtContactNumber,
                            txtUsername,
                            txtPassword,
                            txtConfirmPassword
                    );
                    rbFemale.setSelected(true);
                    cboEducation.setSelectedIndex(0);
                })
        );


        addBackListener(btnBack);

    }
}
