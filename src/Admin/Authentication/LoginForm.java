package Admin.Authentication;

import Admin.Dashboard;
import Database.generated.Routines;
import Utils.*;

import javax.swing.*;

import static Admin.Application.Main.activeUserId;
import static Admin.Application.Main.activeUsername;
import static Admin.Application.Main.database;

public class LoginForm extends SmartJFrame {
    private JPanel view;
    private JTextField txtUsername;
    private JButton btnLogin;
    private JButton btnSignUp;
    private JPasswordField txtPassword;

    public LoginForm() {
        super(null);
        initializeForm();
        setTitle("Login Form");
        setContentPane(view);
        setSize(400,210);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setLocationRelativeTo(null);
        setResizable(false);
        setVisible(true);
    }

    public void initializeForm() {
        btnLogin.addActionListener(e -> {
            String authId = database.context().select(
                    Routines.authenticate(txtUsername.getText(), Hasher.hash(new String(txtPassword.getPassword())))
            ).fetchOne().getValue(0).toString();
            if (!"-1".equals(authId)) {
                activeUserId = authId;
                activeUsername = txtUsername.getText();
                ControlUtils.clearControls(txtUsername, txtPassword);
                FormSwitching.switchForm(LoginForm.this, Dashboard.class);
            } else {
                DialogUtils.showWarningMsg(LoginForm.this, "Username or password is invalid.");
            }
        });

        btnSignUp.addActionListener(e -> {
            ControlUtils.clearControls(txtUsername, txtPassword);
            FormSwitching.switchForm(LoginForm.this, SignUpForm.class);
        });
    }
}
