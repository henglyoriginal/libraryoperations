# Library Operations

Library Operations is one of my first *Java Swing* project ever which is the assignment of third year's *Java Programming* subject in **Royal University of Phnom Penh**.

# Features!

  - CRUD on Employees, Members, Items
  - Perform buy-book and loan/return transaction
  - Generate reports

# Installation

#### Requirement
- IntelliJ Idea IDE (https://www.jetbrains.com/idea/download/)
- MySql 8.0.15 (https://dev.mysql.com/downloads/installer/)
- JDBC Driver for MySQL | Connector/J (https://dev.mysql.com/downloads/connector/j/)
- JDK 1.8 (https://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)

#### Step to set-up project
1. Run project_sql.sql in MySql console (Shell) - Create database, tables and input some default data
2. In _.\LibraryOperationsMisc_ directory
    1. Edit content in *library.xml.example* by replace needed info like YOUR_DB_HOST, YOUR_DB_USERNAME, YOUR_DB_PASSWORD
    2. Rename *library.xml.example* to *library.xml*
    3. Double click on *script.bat* - Generate classes and methods from database
3.  Edit content in  _.\src\Database\ExampleDatabaseConfiguration.java_  by replace needed info like {db_host}, {db_username}, {db_password} and rename _ExampleDatabaseConfiguration.java_ to _DatabaseConfiguration.java_
4. In Project Structure of IntelliJ (Ctrl+Alt+Shift+S or File->Project Structure...), 
 Under Project Settings -> Libraries -> "+" -> Java (from dropdown) -> _.\lib_ -> Select all _*.jar_ -> OK -> OK
5. Now the project should be able to run!

